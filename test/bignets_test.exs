defmodule BignetsTest do
  use ExUnit.Case
  doctest Bignets

  @tag :skip
  test "Test1. simple gossip epidemic dissemination" do
    Mix.Tasks.Bignets.Sim.run(["Bignets.Examples.Gossip", "--nproc", "10000"])
  end

  @tag :skip
  test "Test2. GPS paper" do
    Mix.Tasks.Bignets.Sim.run(["Bignets.Examples.GPS", "--nproc", "10000", "--sync", "--tmax", "20", "--", "--nprimaries", "1000"])
  end

  @tag :skip
  test "Test3. MST Event Store" do
    Mix.Tasks.Bignets.Sim.run(["--nproc", "1000", "--tmax", "4000", "--",
      "Exp.EventStore.MST", "--evranditv", "100", "--evminitv", "100",
      "--lastevent", "2000"])
  end

  @tag :skip
  test "Test4. MPT Event Store" do
    Mix.Tasks.Bignets.Sim.run(["--nproc", "1000", "--tmax", "4000", "--",
      "Exp.EventStore.MST", "--evranditv", "100", "--evminitv", "100",
      "--lastevent", "2000", "--treemodule", "MerklePrefixTree"])
  end

  @tag :skip
  test "Test5. Scuttlebutt" do
    Mix.Tasks.Bignets.Sim.run(["--nproc", "1000", "--tmax", "4000", "--",
      "Exp.EventStore.ScuttleButt", "--evranditv", "100", "--evminitv", "100",
      "--lastevent", "2000"])
  end
end
