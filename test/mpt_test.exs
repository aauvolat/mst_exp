defmodule BignetsTest.MPT do
  use ExUnit.Case
  doctest Bignets

  test "MPT" do
    alias MST.MerklePrefixTree, as: MPT

    a = Enum.reduce(1..10, %MPT{}, &(MPT.insert(&2, &1)))
    b = Enum.reduce(5..15, %MPT{}, &(MPT.insert(&2, &1)))
    z = Enum.reduce(1..15, %MPT{}, &(MPT.insert(&2, &1)))
    c = MPT.merge(a, b)
    d = MPT.merge(b, a)
    assert c.root == d.root
    assert c.root == z.root
    MPT.dump(c)

    da = MPT.diff_to_list(c, a) |> Enum.map(&(elem(&1, 0)))
    assert Enum.sort(da) == Enum.sort(Enum.to_list(11..15))
    db = MPT.diff_to_list(c, b) |> Enum.map(&(elem(&1, 0)))
    assert Enum.sort(db) == Enum.sort(Enum.to_list(1..4))

    assert MPT.diff_to_list(a, c) == []
    assert MPT.diff_to_list(b, c) == []

    dba = MPT.diff_to_list(b, a) |> Enum.map(&(elem(&1, 0)))
    assert Enum.sort(dba) == Enum.to_list(11..15)
    dab = MPT.diff_to_list(a, b) |> Enum.map(&(elem(&1, 0)))
    assert Enum.sort(dab) == Enum.to_list(1..4)
  end

  test "MPT 2" do
    alias MST.MerklePrefixTree, as: MPT

    a = Enum.reduce(Enum.shuffle(1..1000), %MPT{}, &(MPT.insert(&2, &1)))
    b = Enum.reduce(Enum.shuffle(550..1500), %MPT{}, &(MPT.insert(&2, &1)))
    z = Enum.reduce(1..1500, %MPT{}, &(MPT.insert(&2, &1)))
    c = MPT.merge(a, b)
    d = MPT.merge(b, a)
    assert c.root == d.root
    assert c.root == z.root

    dca = MPT.diff_to_list(c, a) |> Enum.map(&(elem(&1, 0)))
    assert Enum.sort(dca) == Enum.to_list(1001..1500)
    dcb = MPT.diff_to_list(c, b) |> Enum.map(&(elem(&1, 0)))
    assert Enum.sort(dcb) == Enum.to_list(1..549)

    assert MPT.diff_to_list(a, c) == []
    assert MPT.diff_to_list(b, c) == []

    dba = MPT.diff_to_list(b, a) |> Enum.map(&(elem(&1, 0)))
    assert Enum.sort(dba) == Enum.to_list(1001..1500)
    dab = MPT.diff_to_list(a, b) |> Enum.map(&(elem(&1, 0)))
    assert Enum.sort(dab) == Enum.to_list(1..549)
  end
end
