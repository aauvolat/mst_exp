defmodule BignetsTest.MST do
  use ExUnit.Case
  doctest Bignets

  test "MST" do
    alias MST.MerkleSearchTree, as: MST

    a = Enum.reduce(1..10, %MST{}, &(MST.insert(&2, &1)))
    b = Enum.reduce(5..15, %MST{}, &(MST.insert(&2, &1)))
    z = Enum.reduce(1..15, %MST{}, &(MST.insert(&2, &1)))
    c = MST.merge(a, b)
    d = MST.merge(b, a)
    assert c.root == d.root
    assert c.root == z.root
    MST.dump(c)

    da = MST.diff_to_list(c, a) |> Enum.map(&(elem(&1, 0)))
    assert Enum.sort(da) == Enum.sort(Enum.to_list(11..15))
    db = MST.diff_to_list(c, b) |> Enum.map(&(elem(&1, 0)))
    assert Enum.sort(db) == Enum.sort(Enum.to_list(1..4))

    assert MST.diff_to_list(a, c) == []
    assert MST.diff_to_list(b, c) == []

    dba = MST.diff_to_list(b, a) |> Enum.map(&(elem(&1, 0)))
    assert Enum.sort(dba) == Enum.to_list(11..15)
    dab = MST.diff_to_list(a, b) |> Enum.map(&(elem(&1, 0)))
    assert Enum.sort(dab) == Enum.to_list(1..4)
  end

  test "MST 2" do
    alias MST.MerkleSearchTree, as: MST

    a = Enum.reduce(Enum.shuffle(1..1000), %MST{}, &(MST.insert(&2, &1)))
    b = Enum.reduce(Enum.shuffle(550..1500), %MST{}, &(MST.insert(&2, &1)))
    z = Enum.reduce(1..1500, %MST{}, &(MST.insert(&2, &1)))
    c = MST.merge(a, b)
    d = MST.merge(b, a)
    assert c.root == d.root
    assert c.root == z.root

    assert Enum.to_list(1..1500) == (MST.to_list(c) |> Enum.map(&(elem(&1, 0))))

    dca = MST.diff_to_list(c, a) |> Enum.map(&(elem(&1, 0)))
    assert dca == Enum.to_list(1001..1500)
    dcb = MST.diff_to_list(c, b) |> Enum.map(&(elem(&1, 0)))
    assert dcb == Enum.to_list(1..549)

    assert MST.diff_to_list(a, c) == []
    assert MST.diff_to_list(b, c) == []

    dba = MST.diff_to_list(b, a) |> Enum.map(&(elem(&1, 0)))
    assert dba == Enum.to_list(1001..1500)
    dab = MST.diff_to_list(a, b) |> Enum.map(&(elem(&1, 0)))
    assert dab == Enum.to_list(1..549)
  end
end
