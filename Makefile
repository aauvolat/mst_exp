all: exp/test_mst.csv exp/test_mpt.csv exp/test_sb.csv

exp/test_mst.csv:
	mix bignets.sim --out $@ --nproc 100 --tmax 4000 -- Exp.EventStore.MST --evranditv 1 --evminitv 100 --lastevent 2000

exp/test_mpt.csv:
	mix bignets.sim --out $@ --nproc 100 --tmax 4000 -- Exp.EventStore.MST --evranditv 1 --evminitv 100 --treemodule MerklePrefixTree --lastevent 2000

exp/test_sb.csv:
	mix bignets.sim --out $@ --nproc 100 --tmax 4000 -- Exp.EventStore.ScuttleButt --evranditv 1 --evminitv 100 --lastevent 2000
