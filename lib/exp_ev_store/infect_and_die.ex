defmodule Exp.EventStore.InfectAndDie do
  alias MST.MerkleList, as: ML

  defmodule State do
    defstruct items: ML.new(&MST.cmp_term/2),
              items_set: MapSet.new(),
              opts: []
  end

  def parse_args(args) do
    {opts, r, inv} = OptionParser.parse(args, strict: [
      nevgenerators: :integer,
      evminitv: :integer,
      evranditv: :integer,
      lastevent: :integer,
      fanout: :integer,
      gossipitv: :interger,   ## not used, TODO use it?
    ])
    if r != [] or inv != [] do
      IO.puts("Invalid arguments for #{inspect __MODULE__}")
      System.halt(1)
    end
    opts = put_in(opts[:fanout], opts[:fanout] || 6)
    opts = put_in(opts[:evminitv], opts[:evminitv] || 1000)
    opts = put_in(opts[:evranditv], opts[:evranditv] || 10000)
    opts = put_in(opts[:nevgenerators], opts[:nevgenerators] || 10)
    opts
  end

  def init(node_id, net, opts) do
    state = %State{opts: opts}
    net = if node_id < opts[:nevgenerators] do
      Bignets.Network.send(net, net.my_id, :gen_event, state.opts[:evminitv]+:rand.uniform(state.opts[:evranditv]))
    else
      net
    end
    {state, net}
  end

  def handle(state, :gen_event, net) do
    if state.opts[:lastevent] == nil or net.time <= state.opts[:lastevent] do
      event = {net.time, net.my_id}
      state = %{state | items: ML.insert(state.items, event),
                        items_set: MapSet.put(state.items_set, event)}
      net = Bignets.Network.send(net, net.my_id, :gen_event, :rand.uniform(state.opts[:evranditv]))
      net = Bignets.Network.save_metric(net, "n_events", Bignets.Metric.Sum.value(1))
      gossip(state, net, event)
    else
      {state, net}
    end
  end

  def handle(state, {:gossip, event}, net) do
    if event in state.items_set do
      net = Bignets.Network.save_metric(net, "n_gossip-", Bignets.Metric.Sum.value(1))
      {state, net}
    else
      net = Bignets.Network.save_metric(net, "n_gossip+", Bignets.Metric.Sum.value(1))
      state = %{state | items: ML.insert(state.items, event),
                        items_set: MapSet.put(state.items_set, event)}
      gossip(state, net, event)
    end
  end

  def gossip(state, net, event) do
    peers = Bignets.Network.sample_peers(net, state.opts[:fanout])
    net = Enum.reduce(peers, net, fn(peer, net) ->
      Bignets.Network.send(net, peer, {:gossip, event}) end)
    {state, net}
  end

  def metrics(state, net) do
    state_id = state.items.top
    net
    |> Bignets.Network.save_metric("n_events", Bignets.Metric.Sum.value(0))
    |> Bignets.Network.save_metric("n_gossip+", Bignets.Metric.Sum.value(0))
    |> Bignets.Network.save_metric("n_gossip-", Bignets.Metric.Sum.value(0))
    |> Bignets.Network.save_metric("n_roots", Bignets.Metric.CountDistinct.item(state_id))
  end
end
