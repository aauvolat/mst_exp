defmodule Exp.EventStore.ScuttleButt do
  defmodule State do
    defstruct last_clock: %{},
              t_last_gossip: 0,
              opts: nil,
              events: %{}
  end

  def parse_args(args) do
    {opts, r, inv} = OptionParser.parse(args, strict: [
      nevgenerators: :integer,
      evminitv: :integer,
      evranditv: :integer,
      gossipitv: :integer,
      lastevent: :integer,
      fanout: :integer,
    ])
    if r != [] or inv != [] do
      IO.puts("Invalid arguments for #{inspect __MODULE__}")
      System.halt(1)
    end
    opts = put_in(opts[:fanout], opts[:fanout] || 6)
    opts = put_in(opts[:gossipitv], opts[:gossipitv] || 100)
    opts = put_in(opts[:evminitv], opts[:evminitv] || 1000)
    opts = put_in(opts[:evranditv], opts[:evranditv] || 10000)
    opts = put_in(opts[:nevgenerators], opts[:nevgenerators] || 10)
    opts = put_in(opts[:maxmerges], opts[:maxmerges] || 6)
    opts
  end

  def init(node_id, net, opts) do
    state = %State{opts: opts}
    net = if node_id < opts[:nevgenerators] do
      Bignets.Network.send(net, net.my_id, :gen_event, state.opts[:evminitv]+:rand.uniform(state.opts[:evranditv]))
    else
      net
    end
    {state, net}
  end

  def handle(state, :gen_event, net) do
    if state.opts[:lastevent] == nil or net.time <= state.opts[:lastevent] do
      event = {net.time, net.my_id}

      net = Bignets.Network.send(net, net.my_id, :gen_event, :rand.uniform(state.opts[:evranditv]))
      net = Bignets.Network.save_metric(net, "n_events", Bignets.Metric.Sum.value(1))
      net = Bignets.Network.save_metric(net, "entropy", Bignets.Metric.DisseminationEntropyDelta.value([event]))
      log_recv_events(net, [event])

      state = %{state | events: Map.put(state.events, net.my_id, [event | (state.events[net.my_id] || [])])}
      state = put_in(state.last_clock[net.my_id], net.time)

      maybe_gossip(state, net)
    else
      {state, net}
    end
  end

  def handle(state, {:gossip, who, in_events, their_last_clock, n}, net) do
    # 1. Integrate incoming events
    {state, new, net} = if in_events != nil do
      c_in_events = Enum.reduce(in_events, %{}, fn {t, id}, evs ->
        Map.put(evs, id, if evs[id] == nil do [{t, id}] else [{t, id} | evs[id]] end)
      end)
      {new_st_evs, new_st_clk, net} = Enum.reduce(c_in_events, {state.events, state.last_clock, net},
        fn {id, evs}, {evx, clx, net} ->
          evs = Enum.sort_by(evs, fn {t, _} -> -t end)
          [{last_t, _} | _] = evs
          case evx[id] do
            nil ->
              log_recv_events(net, evs)
              net = Bignets.Network.save_metric(net, "entropy", Bignets.Metric.DisseminationEntropyDelta.value(evs))
              { Map.put(evx, id, evs), Map.put(clx, id, last_t), net }
            [{t0, _} | _] = prev when last_t > t0 ->
              keep = Enum.take_while(evs, fn {t, _} -> t > t0 end)
              log_recv_events(net, keep)
              net = Bignets.Network.save_metric(net, "entropy", Bignets.Metric.DisseminationEntropyDelta.value(keep))
              { Map.put(evx, id, keep ++ prev), Map.put(clx, id, last_t), net }
            _ -> {evx, clx, net}
          end
        end)
      new = state.last_clock != new_st_clk
      { %{state | events: new_st_evs, last_clock: new_st_clk}, new, net }
    else
      {state, false, net}
    end
    # 1bis. Propagate if new events were received
    {state, net} = if new do maybe_gossip(state, net) else {state, net} end
    # 2. Handle their last clock
    if n < 2 and their_last_clock != nil do
      out_events = state.events
               |> Enum.map(fn {_peer, evs} ->
                 Enum.take_while(evs, fn {time, id} -> their_last_clock[id] == nil or time > their_last_clock[id] end)
                end)
              |> Enum.reduce([], &++/2)
      missing = Enum.any?(their_last_clock, fn {id, clk} -> state.last_clock[id] == nil or clk > state.last_clock[id] end)
      if out_events != [] or missing do
        net = Bignets.Network.send(net, who, {:gossip, net.my_id, out_events, if missing do state.last_clock else nil end, n+1})
        {state, net}
      else
        {state, net}
      end
    else
      {state, net}
    end
  end

  def handle(state, {:try_gossip_again, t}, net) do
    if t > state.t_last_gossip do
      maybe_gossip(state, net)
    else
      {state, net}
    end
  end

  def metrics(state, net) do
    # all_events = state.events
    #            |> Enum.reduce([], fn {_, x}, acc -> x ++ acc end)
    # state_id = all_events
    #            |> Enum.sort()
    #            |> MST.term_hash()
    net
    # |> Bignets.Network.save_metric("n_roots", Bignets.Metric.CountDistinct.item(state_id))
    |> Bignets.Network.save_metric("n_events", Bignets.Metric.Sum.value(0))
    |> Bignets.Network.save_metric("entropy", Bignets.Metric.DisseminationEntropyDelta.value([]))
    # |> Bignets.Network.save_metric("ref_entropy", Bignets.Metric.DisseminationEntropy.value(all_events))
  end

  defp log_recv_events(net, events) do
    list = for {evtime, evpeer} <- events do
      [net.my_id, net.time, evpeer, evtime]
    end
    GenServer.cast(:app_logger, {:log_many, list})
  end

  defp maybe_gossip(state, net) do
    if net.time >= state.t_last_gossip + state.opts[:gossipitv] do
      peers = Bignets.Network.sample_peers(net, state.opts[:fanout])
      net = Enum.reduce(peers, net, fn(peer, net) ->
        Bignets.Network.send(net, peer, {:gossip, net.my_id, nil, state.last_clock, 0}) end)
      state = %{state | t_last_gossip: net.time}
      {state, net}
    else
      # remind ourself to do it at some later time
      net = Bignets.Network.send(net, net.my_id, {:try_gossip_again, net.time}, state.opts[:gossipitv])
      {state, net}
    end
  end
end
