defmodule Exp.EventStore.RumorMongering do
  alias MST.MerkleList, as: ML

  defmodule State do
    defstruct items_set: MapSet.new(),
              items: ML.new(&MST.cmp_term/2),
              hot: [],
              opts: [],
              t_last_gossip: 0
  end

  def parse_args(args) do
    {opts, r, inv} = OptionParser.parse(args, strict: [
      nevgenerators: :integer,
      evminitv: :integer,
      evranditv: :integer,
      lastevent: :integer,
      fanout: :integer,
      timeout: :integer,
      gossipitv: :integer,
    ])
    if r != [] or inv != [] do
      IO.puts("Invalid arguments for #{inspect __MODULE__}")
      System.halt(1)
    end
    opts = put_in(opts[:fanout], opts[:fanout] || 6)
    opts = put_in(opts[:evminitv], opts[:evminitv] || 1000)
    opts = put_in(opts[:evranditv], opts[:evranditv] || 10000)
    opts = put_in(opts[:nevgenerators], opts[:nevgenerators] || 10)
    opts = put_in(opts[:gossipitv], opts[:gossipitv] || 100)
    opts = put_in(opts[:timeout], opts[:timeout] || 5000)
    opts
  end

  def init(node_id, net, opts) do
    state = %State{opts: opts}
    net = if node_id < opts[:nevgenerators] do
      Bignets.Network.send(net, net.my_id, :gen_event, state.opts[:evminitv]+:rand.uniform(state.opts[:evranditv]))
    else
      net
    end
    {state, net}
  end

  def handle(state, :gen_event, net) do
    if state.opts[:lastevent] == nil or net.time <= state.opts[:lastevent] do
      event = {net.time, net.my_id}
      state = %{state | 
        items_set: MapSet.put(state.items_set, event),
        items: ML.insert(state.items, event),
        hot: [{net.time, event} | state.hot]
      }
      net = Bignets.Network.send(net, net.my_id, :gen_event, :rand.uniform(state.opts[:evranditv]))
      net = Bignets.Network.save_metric(net, "n_events", Bignets.Metric.Sum.value(1))
      maybe_gossip(state, net)
    else
      {state, net}
    end
  end

  def handle(state, {:gossip, hotlist}, net) do
    hot_new = Enum.filter(hotlist, &(&1 not in state.items_set))
    net = Bignets.Network.save_metric(net, "n_i+", Bignets.Metric.Sum.value(Enum.count(hot_new)))
    net = Bignets.Network.save_metric(net, "n_i-", Bignets.Metric.Sum.value(Enum.count(hotlist) - Enum.count(hot_new)))
    if hot_new != [] do
      net = Bignets.Network.save_metric(net, "n_gossip+", Bignets.Metric.Sum.value(1))
      state = %{state |
        items_set: Enum.reduce(hot_new, state.items_set, &(MapSet.put(&2, &1))),
        items: Enum.reduce(hot_new, state.items, &(ML.insert(&2, &1))),
        hot: Enum.map(hot_new, &({net.time, &1})) ++ state.hot}
      maybe_gossip(state, net)
    else
      net = Bignets.Network.save_metric(net, "n_gossip-", Bignets.Metric.Sum.value(1))
      {state, net}
    end
  end

  def handle(state, {:try_gossip_again, next_time}, net) do
    if next_time > state.t_last_gossip do
      maybe_gossip(state, net)
    else
      {state, net}
    end
  end

  def maybe_gossip(state, net) do
    state = %{state | hot: state.hot |> Enum.filter(fn {t, _} -> t >= net.time - state.opts[:timeout] end)}
    if net.time > state.t_last_gossip + state.opts[:gossipitv] do
      if state.hot != [] do
        hotlist = for {_, it} <- state.hot, do: it
        peers = Bignets.Network.sample_peers(net, state.opts[:fanout])
        net = Enum.reduce(peers, net, fn(peer, net) ->
          Bignets.Network.send(net, peer, {:gossip, hotlist}) end)
        state = %{state | t_last_gossip: net.time}
        {state, net}
      else
        {state, net}
      end
    else
      net = Bignets.Network.send(net, net.my_id, {:try_gossip_again, net.time}, state.opts[:gossipitv])
      {state, net}
    end
  end

  def metrics(state, net) do
    state_id = state.items.top
    net
    |> Bignets.Network.save_metric("n_events", Bignets.Metric.Sum.value(0))
    |> Bignets.Network.save_metric("n_gossip+", Bignets.Metric.Sum.value(0))
    |> Bignets.Network.save_metric("n_gossip-", Bignets.Metric.Sum.value(0))
    |> Bignets.Network.save_metric("n_roots", Bignets.Metric.CountDistinct.item(state_id))
  end
end
