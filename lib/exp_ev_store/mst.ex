defmodule Exp.EventStore.MST do
  @moduledoc"""
  A situation where we use the Merkle Search Tree to build
  a decentralized event store.

  Suitable for simulation with Bignets.
  """

  defmodule State do
    defstruct t_last_gossip: 0,
              current: nil,
              opts: nil,
              treemod: nil,
              history: [],
              merges: %{}
  end

  def parse_args(args) do
    {opts, r, inv} = OptionParser.parse(args, strict: [
      nevgenerators: :integer,
      evminitv: :integer,
      evranditv: :integer,
      gossipitv: :integer,
      lastevent: :integer,
      fanout: :integer,
      maxmerges: :integer,
      maxsamemerge: :integer,
      backlog: :integer,
      treemodule: :string,
      nosinglegossip: :boolean,
    ])
    if r != [] or inv != [] do
      IO.puts("Invalid arguments for #{inspect __MODULE__}")
      System.halt(1)
    end
    opts = put_in(opts[:fanout], opts[:fanout] || 6)
    opts = put_in(opts[:gossipitv], opts[:gossipitv] || 100)
    opts = put_in(opts[:evminitv], opts[:evminitv] || 1000)
    opts = put_in(opts[:evranditv], opts[:evranditv] || 10000)
    opts = put_in(opts[:nevgenerators], opts[:nevgenerators] || 10)
    opts = put_in(opts[:maxmerges], opts[:maxmerges] || 6)
    opts = put_in(opts[:maxsamemerge], opts[:maxsamemerge] || 1)
    opts = put_in(opts[:backlog], opts[:backlog] || 10000)
    opts = put_in(opts[:treemodule], opts[:treemodule] || "MerkleSearchTree")
    true = (opts[:treemodule] in ["MerkleSearchTree", "MerklePrefixTree"])
    opts
  end

  def init(node_id, net, opts) do
    treemod = ("Elixir.MST." <> opts[:treemodule]) |> String.to_atom
    state = %State{
      opts: opts,
      treemod: treemod,
      current: apply(treemod, :new, []),
    }
    net = if node_id < opts[:nevgenerators] do
      Bignets.Network.send(net, net.my_id, :gen_event, state.opts[:evminitv]+:rand.uniform(state.opts[:evranditv]))
    else
      net
    end
    {state, net}
  end

  def handle(state, :gen_event, net) do
    if state.opts[:lastevent] == nil or net.time <= state.opts[:lastevent] do
      event = {net.time, net.my_id}
      state = %{state | current: apply(state.treemod, :insert, [state.current, event])}
      state = %{state | history: [{net.time, state.current.root} | state.history] |> Enum.filter(fn {t, _} -> t >= net.time - state.opts[:backlog] end)}
      true = (MST.PageStore.missing_set(state.current.store, state.current.root) == MapSet.new())
      net = Bignets.Network.send(net, net.my_id, :gen_event, :rand.uniform(state.opts[:evranditv]))
      net = Bignets.Network.save_metric(net, "n_events", Bignets.Metric.Sum.value(1))
      log_recv_events(net, [event])
      net = Bignets.Network.save_metric(net, "entropy", Bignets.Metric.DisseminationEntropyDelta.value([event]))
      if state.opts[:nosinglegossip] do
        maybe_gossip(state, net)
      else
        maybe_gossip(state, net, event)
      end
    else
      {state, net}
    end
  end

  def handle(state, {:try_gossip_again, t}, net) do
    if t > state.t_last_gossip do
      maybe_gossip(state, net)
    else
      {state, net}
    end
  end

  def handle(state, {:gossip, from, root, event}, net) do
    if root == state.current.root do
      net = Bignets.Network.save_metric(net, "n_gossip-", Bignets.Metric.Sum.value(1))
      {state, net}
    else
      {state, net, is_missing} = if event != nil do
        prev_root = state.current.root
        state = %{state | current: apply(state.treemod, :insert, [state.current, event])}
        if state.current.root != prev_root do
          net = Bignets.Network.save_metric(net, "n_gossip+", Bignets.Metric.Sum.value(1))
          log_recv_events(net, [event])
          net = Bignets.Network.save_metric(net, "entropy", Bignets.Metric.DisseminationEntropyDelta.value([event]))
          new_merges = for {k, v} <- state.merges, v != state.current.root, into: %{}, do: {k, v}
          state = put_in(state.merges, new_merges)
          {state, net} = maybe_gossip(state, net, event)
          {state, net, state.current.root != root}
        else
          net = Bignets.Network.save_metric(net, "n_gossip-", Bignets.Metric.Sum.value(1))
          {state, net, true}
        end
      else
        {state, net, true}
      end
      if is_missing do
        maybe_merge(state, net, from, root)
      else
        {state, net}
      end
    end
  end

  def handle(state, {:get, from, set, lastroot}, net) do
    missing = for k <- set, not MST.PageStore.has?(state.current.store, k), do: k

    net = if missing == [] do
      answer = for k <- set, MST.PageStore.has?(state.current.store, k), into: %{},
        do: {k, MST.PageStore.get(state.current.store, k)}
      Bignets.Network.send(net, from, {:put, net.my_id, answer})
    else
      Bignets.Network.send(net, from, {:missing, net.my_id})
    end

    if lastroot == state.current.root do
      {state, net}
    else
       maybe_merge(state, net, from, lastroot)
    end
  end

  def handle(state, {:put, from, set}, net) do
    if Map.has_key?(state.merges, from) do
      # Metric: count useless transfers
      n_useless_pages = Enum.reduce(set, 0, fn {k, _}, acc -> acc + (if MST.PageStore.has?(state.current.store, k) do 1 else 0 end) end)
      n_usefull_pages = Enum.reduce(set, 0, fn {k, _}, acc -> acc + (if MST.PageStore.has?(state.current.store, k) do 0 else 1 end) end)
      net = Bignets.Network.save_metric(net, "n_pages-", Bignets.Metric.Sum.value(n_useless_pages))
      net = Bignets.Network.save_metric(net, "n_pages+", Bignets.Metric.Sum.value(n_usefull_pages))

      new_store = Enum.reduce(set, state.current.store,
        fn {h, p}, acc ->
          {^h, store} = MST.PageStore.put(acc, p)
          store
        end)
      state = put_in(state.current.store, new_store)
      continue_merge(state, net, from)
    else
      net = Bignets.Network.save_metric(net, "n_pages-", Bignets.Metric.Sum.value(map_size set))
      {state, net}
    end
  end

  def handle(state, {:missing, from}, net) do
    if Map.has_key?(state.merges, from) do
      # abandon merge
      net = Bignets.Network.save_metric(net, "n_abandons", Bignets.Metric.Sum.value(1))
      state = put_in(state.merges, Map.delete(state.merges, from))
      {state, net}
    else
      {state, net}
    end
  end

  defp maybe_merge(state, net, from, root) do
    nsources = (for {_, v} <- state.merges, v == root, do: true) |> Enum.count()
    if root != nil and nsources < state.opts[:maxsamemerge] and map_size(state.merges) < state.opts[:maxmerges] do
      state = put_in(state.merges[from], root)
      continue_merge(state, net, from)
    else
      {state, net}
    end
  end

  defp continue_merge(state, net, from) do
    root = state.merges[from]
    true = (root != nil)

    reqset = MST.PageStore.missing_set(state.current.store, root)
    if MapSet.size(reqset) != 0 do
      net = Bignets.Network.send(net, from, {:get, net.my_id, reqset, state.current.root})
      {state, net}
    else
      prev_root = state.current.root
      state = %{state | current: apply(state.treemod, :merge, [state.current, %{state.current | root: root}])}
      true = (MST.PageStore.missing_set(state.current.store, state.current.root) == MapSet.new())

      new_merges = for {k, v} <- state.merges, v != root and v != state.current.root, into: %{}, do: {k, v}
      state = put_in(state.merges, new_merges)

      if prev_root != state.current.root do
        new_events = apply(state.treemod, :diff_to_list, [state.current, %{state.current | root: prev_root}])
        new_events_x = Enum.map(new_events, &(elem(&1, 0)))
        log_recv_events(net, new_events_x)
        net = Bignets.Network.save_metric(net, "entropy", Bignets.Metric.DisseminationEntropyDelta.value(new_events_x))
        state = %{state | history: [{net.time, state.current.root} | state.history] |> Enum.filter(fn {t, _} -> t >= net.time - 10000 end)}
        net = Bignets.Network.save_metric(net, "n_merges+", Bignets.Metric.Sum.value(1))
        # IO.puts("#{net.my_id}: #{(prev_root || <<>>) |> Base.encode16 |> String.slice(0..8)} -> #{state.current.root |> Base.encode16 |> String.slice(0..8)}")
        merge_roots = for {_, v} <- state.merges, do: v
        keep_roots = merge_roots ++ Enum.map(state.history, &(elem(&1, 1)))
        state = put_in(state.current.store, MST.PageStore.gc(state.current.store, keep_roots))
        net = if state.current.root != root do
          Bignets.Network.send(net, from, {:gossip, net.my_id, state.current.root, nil})
        else
          net
        end
        maybe_gossip(state, net)
      else
        net = Bignets.Network.save_metric(net, "n_merges-", Bignets.Metric.Sum.value(1))
        {state, net}
      end
    end
  end

  defp log_recv_events(net, events) do
    list = for {evtime, evpeer} <- events do
      [net.my_id, net.time, evpeer, evtime]
    end
    GenServer.cast(:app_logger, {:log_many, list})
  end

  defp maybe_gossip(state, net, event \\ nil) do
    # IO.puts("#{net.my_id}: !! #{(state.current.root || <<>>) |> Base.encode16 |> String.slice(0..8)}")
    if event != nil or net.time >= state.t_last_gossip + state.opts[:gossipitv] do
      peers = Bignets.Network.sample_peers(net, state.opts[:fanout])
      net = Enum.reduce(peers, net, fn(peer, net) ->
        Bignets.Network.send(net, peer, {:gossip, net.my_id, state.current.root, event}) end)
      state = %{state | t_last_gossip: net.time}
      {state, net}
    else
      # remind ourself to do it at some later time
      net = Bignets.Network.send(net, net.my_id, {:try_gossip_again, net.time}, state.opts[:gossipitv])
      {state, net}
    end
  end

  def metrics(state, net) do
    net
    |> Bignets.Network.save_metric("n_pending_merges", Bignets.Metric.Sum.value(map_size state.merges))
    |> Bignets.Network.save_metric("n_events", Bignets.Metric.Sum.value(0))
    |> Bignets.Network.save_metric("n_merges+", Bignets.Metric.Sum.value(0))
    |> Bignets.Network.save_metric("n_merges-", Bignets.Metric.Sum.value(0))
    |> Bignets.Network.save_metric("n_pages+", Bignets.Metric.Sum.value(0))
    |> Bignets.Network.save_metric("n_pages-", Bignets.Metric.Sum.value(0))
    |> Bignets.Network.save_metric("n_gossip+", Bignets.Metric.Sum.value(0))
    |> Bignets.Network.save_metric("n_gossip-", Bignets.Metric.Sum.value(0))
    |> Bignets.Network.save_metric("n_abandons", Bignets.Metric.Sum.value(0))
    |> Bignets.Network.save_metric("n_roots", Bignets.Metric.CountDistinct.item(state.current.root))
    |> Bignets.Network.save_metric("entropy", Bignets.Metric.DisseminationEntropyDelta.value([]))
    # |> Bignets.Network.save_metric("ref_entropy", Bignets.Metric.DisseminationEntropy.value(apply(state.treemod, :to_list, [state.current])))
  end
end
