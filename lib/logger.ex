defmodule Bignets.FileLogger do
  use GenServer

  def start_link(filename, opts) do
    GenServer.start_link(__MODULE__, filename, opts)
  end

  def init(filename) do
    {:ok, fh} = File.open(filename, [:write])
    {:ok, fh}
  end

  def handle_cast({:log, x}, fh) do
    line = x |> Enum.map(&to_string/1) |> Enum.join(",")
    IO.write(fh, "#{line}\n")
    {:noreply, fh}
  end

  def handle_cast({:log_many, xx}, fh) do
    for x <- xx do
      handle_cast({:log, x}, fh)
    end
    {:noreply, fh}
  end

  def terminate(_reason, fh) do
    File.close(fh)
    :normal
  end
end
