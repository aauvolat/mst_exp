defmodule Bignets do
  @moduledoc """
  Bignets is an efficient simulator for networks with asynchronous communication and arbitrary latencies.

  Nodes in the system are known from the start and are numbered.

  A program running at a node is a transition function that is executed
  each time a message is running. This function calculates a new state
  and may send message to other peers.

  First version do not implement:
  - pipelining
  """

  defprotocol Network do
    @doc"""
    Returns the identifiers for k random peers.
    """
    def sample_peers(net, k, range \\ nil)

    @doc"""
    Send a message to a peer. This mutates the state of the object
    and returns a new net to use afterwards.
    """
    def send(net, peer_id, msg, delay \\ 0)
  
    @doc"""
    Save an application-specific metric for the current step.
    """
    def save_metric(net, name, metric)
  end

  defmodule SimNet do
    # my_id: int
    # nproc: int -- total processes in sytem
    # latencies_fn: fn (int, int) -> int
    # time: int
    # metrics: dict string -> Bignets.Metric impl
    # inbox: list {int, term}
    defstruct [:my_id, :nproc, :latencies_fn, :time, :metrics, :inbox]

    def sample_peers(net, k, range, prev) do
      if MapSet.size(prev) == k do
        prev
      else
        i = case range do
          nil -> :rand.uniform(net.nproc) - 1
          a..b -> a + :rand.uniform(b-a) - 1
        end
        next = if i == net.my_id do prev else MapSet.put(prev, i) end
        sample_peers(net, k, range, next)
      end
    end
    
    defimpl Network do
      def sample_peers(net, k, range) do
        ms = Bignets.SimNet.sample_peers(net, k, range, MapSet.new())
        MapSet.to_list(ms)
      end

      def send(net, peer_id, msg, delay) do
        nbytes = byte_size(:erlang.term_to_binary msg)
        name = "sim#{peer_id}" |> String.to_atom()
        arrival_time = net.time + net.latencies_fn.(net.my_id, peer_id) + delay
        send(name, {:msg, arrival_time, msg})
        GenServer.cast(:message_logger, {:log, [net.my_id, net.time, peer_id, arrival_time, nbytes]})
        net
        |> save_metric("n_sent", Bignets.Metric.Sum.value(1))
        |> save_metric("b_sent", Bignets.Metric.Sum.value(nbytes))
      end

      def save_metric(net, name, metric) do
        agg = case net.metrics[name] do
          nil -> metric
          m1 -> Bignets.Metric.local_aggregate(m1, metric)
        end
        %{ net | metrics: Map.put(net.metrics, name, agg) }
      end
    end
  end

  @doc"""
  Simulate a network for a number of time steps.

  `module` is the name of an Erlang/Elixir module that implements the algorithm.
  It must have the following functions:

    def init(node_id, net, init_param)

  The `net` argument is a struct that implements the `Bignets.Network` and may be used to
  sample some random peers and send messages. The `init_param` is the value passed
  as argument to this function.
  `init` returns the initial state of the node number `node_id` as well as the `net` object
  after using it. Return tuple: {state, net}

    def handle(state, msg, net)

  `handle` handles an incoming message. Same as above, returns {new_state, net}

    def metrics(state, net)

  Called at each delta-T time bin so that metrics can be saved. Returns the mutated net object.
  """
  def sim_net(module, init_param, nproc, latencies_fn, min_latency, t_max) do

    spawn_link(__MODULE__, :process, [self(), 0, nproc, module, init_param, nproc, latencies_fn])

    receive do
      {:sync, :setup} ->
        steps = run_top(0, t_max, nproc, min_latency, [], %{})
        Enum.reverse(steps)
      x ->
        IO.puts("Error: #{inspect x}")
    end
  end

  defp run_top(t, t_max, nproc, min_latency, acc, prev_metrics) do
    send(:sim0, {:start, t})
    receive do
      {:sync, metrics} ->
        metrics = for {k, v} <- metrics, into: %{}, do: {k, Bignets.Metric.prev_aggregate(v, prev_metrics[k], nproc)}
        metrics_val = for {k, v} <- metrics, into: %{}, do: {k, Bignets.Metric.final_value(v, nproc)}
        metrics_str = metrics_val |> Enum.map(fn {k, v} -> "#{k}: #{v}" end) |> Enum.join(", ")
        IO.puts("#{t}: #{metrics_str}")
        acc = [{t, metrics_val} | acc]
        if t >= t_max do
          IO.puts("Time limit reached, stopping.")
          send(:sim0, :done)
          receive do
            {:sync, :done} ->
              acc
          end
        else
          run_top(t + min_latency, t_max, nproc, min_latency, acc, metrics)
        end
      x ->
        IO.puts("Error: unexpected #{inspect x}")
    end
  end

  def process(parent, id, count, module, init_param, nproc, latencies_fn) do
    name = "sim#{id}" |> String.to_atom()
    Process.register(self(), name)

    c_a = div(count - 1, 2)
    c_b = count - 1 - c_a

    ba = if c_a > 0 do
      [spawn_link(__MODULE__, :process, [self(), id+1, c_a, module, init_param, nproc, latencies_fn])]
    else [] end
    bb = if c_b > 0 do
      [spawn_link(__MODULE__, :process, [self(), id+1+c_a, c_b, module, init_param, nproc, latencies_fn])]
    else [] end
    branches = ba ++ bb

    branches |> Enum.map(fn _ ->
        receive do
          {:sync, :setup} -> nil
          x -> IO.puts("Error: unexpected #{inspect x}, expected {:sync, :setup}")
        end
    end)
    send(parent, {:sync, :setup})

    net = %SimNet{my_id: id, nproc: nproc, latencies_fn: latencies_fn, time: 0, inbox: [],
      metrics: %{"n_recv" => Bignets.Metric.Sum.value(0)}}
    receive do
      {:start, 0} ->
        branches |> Enum.map(fn pid -> send(pid, {:start, 0}) end)
        {state, net} = apply(module, :init, [id, net, init_param])
        net = apply(module, :metrics, [state, net])
        run_proc(parent, branches, module, state, net)
    end
  end

  defp run_proc(parent, branches, module, state, net) do
    # A. Communication and synchronization
    # A.1. Receive messages while stuff is happening
    # A.2. As soon as we receive #branches sync messages, aggregate metrics, sync to parent, reset state
    # A.3. Continue receiving messages
    # A.4. Stop doing that as soon as we receive {:start, next_bound} or :done
    # A.5. Propagate :start or :done to children
    if branches == [] do
      send(parent, {:sync, net.metrics})
    end
    {net, bound} = run_sync(parent, branches, [], net)
    if bound != :done do
      # B. Local processing step
      # B.1. Look up messages in inbox that are before the bound
      # B.2. Sort them
      # B.3. Handle them
      {state, net} = run_local(module, state, net, bound)
      # C. Recurse
      run_proc(parent, branches, module, state, net)
    end
  end

  def run_sync(parent, branches, sync_received, net) do
    receive do
      {:msg, t, msg} ->
        net = %{net | inbox: [{t, msg} | net.inbox]}
        run_sync(parent, branches, sync_received, net)
      {:sync, metrics} ->
        sync_received = [metrics | sync_received]
        if Enum.count(sync_received) == Enum.count(branches) do
          metrics = Enum.reduce(sync_received, net.metrics, fn (m, acc) -> metrics_net_agg(m, acc) end)
          send(parent, {:sync, metrics})
        end
        run_sync(parent, branches, sync_received, net)
      {:start, t} ->
        branches |> Enum.map(fn pid -> send(pid, {:start, t}) end)
        {net, t}
      :done ->
        branches |> Enum.map(fn pid -> send(pid, :done) end)
        branches |> Enum.map(fn _ ->
            receive do
              {:sync, :done} -> nil
            end
        end)
        send(parent, {:sync, :done})
        {net, :done}
      x -> IO.puts("Error: unexpected #{inspect x} in run_sync")
    end
  end

  def run_local(module, state, net, bound) do
    msg1 = for {t, msg} <- net.inbox, t <= bound, do: {t, msg}
    msg2 = for {t, msg} <- net.inbox, t > bound, do: {t, msg}
    net = put_in(net.inbox, msg2)
    net = put_in(net.metrics, %{"n_recv" => Bignets.Metric.Sum.value(Enum.count msg1),
      "n_sent" => Bignets.Metric.Sum.value(0),
      "b_sent" => Bignets.Metric.Sum.value(0)})
    msg1 = Enum.sort_by(msg1, fn {t, _msg} -> t end)
    {state, net} = Enum.reduce(msg1, {state, net}, fn ({t, msg}, {state, net}) ->
      net = put_in(net.time, t)
      apply(module, :handle, [state, msg, net])
    end)
    net = apply(module, :metrics, [state, net])
    {state, net}
  end

  # =========
  # AUXILLARY
  # =========

  # defp minx(:inf, :inf), do: :inf
  # defp minx(:inf, n), do: n
  # defp minx(n, :inf), do: n
  # defp minx(n, m), do: min(n, m)

  defp metrics_net_agg(ma, mb) do
    keyset = for k <- Map.keys(ma) ++ Map.keys(mb), do: k
    for k <- keyset, into: %{} do
      case {ma[k], mb[k]} do
        {nil, x} -> {k, x}
        {x, nil} -> {k, x}
        {x, y} -> {k, Bignets.Metric.net_aggregate(x, y)}
      end
    end
  end 
end

