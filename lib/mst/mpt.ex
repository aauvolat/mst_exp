defmodule MST.MerklePrefixTree do
  @moduledoc"""
  A Merkle prefix tree

  A node of the tree is:
      {
        single: nil
        prefix: prefix,
        sub: %{next prefix part: h of subtree, ...}
      }
  or
      {
        single: {key, value},
        prefix: nil,
        sub: nil
      }
  """

  alias MST.PageStore, as: Store

  defstruct root: nil,
            store: MST.LocalStore.new,
            merge: &MST.merge_true/2

  defmodule Page do
    defstruct [:single, :prefix, :sub]
  end

  defimpl MST.Page, for: Page do
    def refs(page) do
      case page.sub do
        nil -> []
        _ ->
          for {_, h} <- page.sub, do: h
      end
    end
  end

  def new() do
    %__MODULE__{}
  end

  def insert(state, key, value \\ true) do
    hkey = hash_key(key)
    {hash, store} = insert_at(state, state.store, state.root, key, hkey, value)
    %{state | root: hash, store: store }
  end

  defp insert_at(s, store, root, key, hkey, value) do
    {new_page, store} = if root == nil do
      { %Page{single: {key, value}, prefix: nil, sub: nil }, store }
    else
      case Store.get(store, root) do
        %Page{single: {skey, svalue}, prefix: nil, sub: nil} ->
          if skey == key do
            vv = s.merge.(value, svalue)
            store = Store.free(store, root)
            { %Page{single: {skey, vv}, prefix: nil, sub: nil}, store }
          else
            hskey = hash_key(skey)
            plen = :binary.longest_common_prefix([hskey, hkey])
            prefix = binary_part(hkey, 0, plen)
            next = binary_part(hkey, plen, 1)
            snext = binary_part(hskey, plen, 1)
            newp1 = %Page{single: {key, value}, prefix: nil, sub: nil}
            {newp1h, store} = Store.put(store, newp1)
            { %Page{single: nil, prefix: prefix, sub: %{next => newp1h, snext => root}}, store }
          end
        %Page{single: nil, prefix: pfx, sub: sub} ->
          plen = :binary.longest_common_prefix([pfx, hkey])
          if plen == byte_size pfx do
            next = binary_part(hkey, plen, 1)
            {newsubnext, store} = insert_at(s, store, sub[next], key, hkey, value)
            sub = put_in(sub[next], newsubnext)
            store = Store.free(store, root)
            { %Page{single: nil, prefix: pfx, sub: sub}, store }
          else
            pfx2 = binary_part(hkey, 0, plen)
            next = binary_part(hkey, plen, 1)
            pnext = binary_part(pfx, plen, 1)
            newp1 = %Page{single: {key, value}, prefix: nil, sub: nil}
            {newp1h, store} = Store.put(store, newp1)
            { %Page{single: nil, prefix: pfx2, sub: %{next => newp1h, pnext => root}}, store }
          end
      end
    end
    Store.put(store, new_page)    # returns {hash, store}
  end

  def merge(to, from) do
    { root, store } = merge_aux(to, from, to.store, to.root, from.root)
    %{ to | store: store, root: root }
  end

  defp merge_aux(s1, s2, store, r1, r2) do
    case {r1, r2} do
      _ when r1 == r2 -> { r1, store }
      {_, nil} ->
        { r1, store }
      {nil, _} ->
        store = Store.copy(store, s2.store, r2)
        { r2, store }
      _ ->
        page1 = Store.get(store, r1)
        page2 = Store.get(store, r2) || Store.get(s2.store, r2)
        case {page1, page2} do
          { %Page{single: {k1, v1}, prefix: nil, sub: nil},
            %Page{single: {k2, v2}, prefix: nil, sub: nil} }
          when k1 == k2 ->
            vnew = s1.merge.(v1, v2)
            page = %Page{single: {k1, vnew}, prefix: nil, sub: nil}
            Store.put(store, page)
          _ ->
            str1 = case page1 do
              %Page{single: {key, _}, prefix: nil, sub: nil} -> hash_key(key)
              %Page{single: nil, prefix: pfx, sub: _} -> pfx
            end
            str2 = case page2 do
              %Page{single: {key, _}, prefix: nil, sub: nil} -> hash_key(key)
              %Page{single: nil, prefix: pfx, sub: _} -> pfx
            end
            plen = :binary.longest_common_prefix([str1, str2])
            pfx = binary_part(str1, 0, plen)
            cond do
              str1 == str2 ->
                %Page{single: nil, prefix: ^pfx, sub: sub1} = page1
                %Page{single: nil, prefix: ^pfx, sub: sub2} = page2
                keyset = MapSet.new(Map.keys(sub1) ++ Map.keys(sub2))
                {store, newsub} = Enum.reduce(keyset, {store, %{}}, fn k, {store, ss} ->
                  {h, store} = merge_aux(s1, s2, store, sub1[k], sub2[k])
                  {store, Map.put(ss, k, h)}
                end)
                page = %Page{single: nil, prefix: pfx, sub: newsub}
                Store.put(store, page)
              str1 == pfx ->
                next2 = binary_part(str2, plen, 1)
                %Page{single: nil, prefix: ^pfx, sub: sub} = page1
                {newsubnext, store} = merge_aux(s1, s2, store, sub[next2], r2)
                page = %Page{single: nil, prefix: pfx, sub: put_in(sub[next2], newsubnext)}
                Store.put(store, page)
              str2 == pfx ->
                next1 = binary_part(str1, plen, 1)
                %Page{single: nil, prefix: ^pfx, sub: sub} = page2
                store = Enum.reduce(sub, store, fn {k, h}, store ->
                  if k != next1 do
                    Store.copy(store, s2.store, h)
                  else
                    store
                  end
                end)
                {newsubnext, store} = merge_aux(s1, s2, store, r1, sub[next1])
                page = %Page{single: nil, prefix: pfx, sub: put_in(sub[next1], newsubnext)}
                Store.put(store, page)
              true ->
                next1 = binary_part(str1, plen, 1)
                next2 = binary_part(str2, plen, 1)
                store = Store.copy(store, s2.store, r2)
                page = %Page{single: nil, prefix: pfx, sub: %{next1 => r1, next2 => r2}}
                Store.put(store, page)
            end
        end
    end
  end

  def to_list(state) do
    to_list(state.store, state.root)
  end
  defp to_list(store, root) do
    case root do
      nil -> []
      _ ->
        case Store.get(store, root) do
          %Page{single: item, prefix: nil, sub: nil} ->
            [item]
          %Page{single: nil, sub: sub} ->
            sub
            |> Enum.map(fn {_, v}  -> to_list(store, v) end)
            |> Enum.reduce([], &++/2)
        end
    end
  end

  @doc"""
  Returns items of arg 1 that are not in arg 2
  """
  def diff_to_list(state1, state2) do
    diff_to_list(state1.store, state1.root, state2.store, state2.root)
  end
  defp diff_to_list(stoa, ra, stob, rb) do
    cond do
      ra == rb -> []
      ra == nil -> []
      rb == nil -> to_list(stoa, ra)
      true ->
        case {Store.get(stoa, ra), Store.get(stob, rb)} do
          { %Page{single: itema, prefix: nil, sub: nil}, %Page{single: itemb, prefix: nil, sub: nil} } -> 
            true = (itema != itemb)
            [itema]
          { %Page{single: nil, prefix: pfxa, sub: suba}, %Page{single: {kb, _}, prefix: nil, sub: nil} } ->
            hkb = hash_key(kb)
            plen = :binary.longest_common_prefix([pfxa, hkb])
            if plen < byte_size(pfxa) do
              to_list(stoa, ra)
            else
              nxtb = binary_part(hkb, plen, 1)
              (for {nxt, hnxt} <- suba, nxt != nxtb, do: to_list(stoa, hnxt))
              |> Enum.reduce(diff_to_list(stoa, suba[nxtb], stob, rb), &++/2)
            end
          { %Page{single: {ka, va}, prefix: nil, sub: nil}, %Page{single: nil, prefix: pfxb, sub: subb} } ->
            hka = hash_key(ka)
            plen = :binary.longest_common_prefix([hka, pfxb])
            if plen < byte_size(pfxb) do
              [{ka, va}]
            else
              nxta = binary_part(hka, plen, 1)
              diff_to_list(stoa, ra, stob, subb[nxta])
            end
          { %Page{single: nil, prefix: pfxa, sub: suba}, %Page{single: nil, prefix: pfxb, sub: subb} } ->
            plen = :binary.longest_common_prefix([pfxa, pfxb])
            cond do
              pfxa == pfxb ->
                Map.keys(suba)
                |> Enum.map(&(diff_to_list(stoa, suba[&1], stob, subb[&1])))
                |> Enum.reduce([], &++/2)
              plen == byte_size(pfxa) ->
                nxtb = binary_part(pfxb, plen, 1)
                (for {nxt, hnxt} <- suba, nxt != nxtb, do: to_list(stoa, hnxt))
                |> Enum.reduce(diff_to_list(stoa, suba[nxtb], stob, rb), &++/2)
              plen == byte_size(pfxb) ->
                nxta = binary_part(pfxa, plen, 1)
                diff_to_list(stoa, ra, stob, subb[nxta])
              true ->
                to_list(stoa, ra)
            end
        end
    end
  end

  def dump(state) do
    dump(state.store, state.root, "")
  end

  defp dump(store, root, lvl) do
    case root do
      nil -> nil
      _ ->
        case Store.get(store, root) do
          %Page{single: {key, value}, prefix: nil, sub: nil} ->
            IO.puts(lvl <> "> #{inspect key} => #{inspect value}")
          %Page{single: nil, prefix: pfx, sub: sub} ->
            IO.puts(lvl <> "> #{inspect pfx}")
            for {p, h} <- sub do
              dump(store, h, lvl <> p)
            end
        end
    end
  end

  defp hash_key(key) do
    key |> MST.term_hash |> Base.encode16
  end
end
