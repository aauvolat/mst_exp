defprotocol MST.Page do
  @moduledoc"""
  Protocol to be implemented by objects that are used as data pages
  in a pagestore and that may reference other data pages by their hash.
  """

  @fallback_to_any true

  @doc"""
  Get hashes of all pages referenced by this page.
  """
  def refs(page)
end

defimpl MST.Page, for: Any do
  def refs(_page), do: []
end


defprotocol MST.PageStore do
  @moduledoc"""
  Protocol to be implemented for page stores to allow their
  manipulation.

  This protocol may also be implemented by store proxies that track
  operations and implement different synchronization or caching mechanisms.
  """

  @doc"""
  Put a page. Argument is the content of the page, returns the
  hash that the store has associated to it.

  Returns {hash, store}
  """
  def put(store, page)

  @doc"""
  Get a page referenced by its hash.

  Returns page
  """
  def get(store, hash)

  @doc"""
  Get the list of pages we know we are missing starting at this root
  """
  def missing_set(store, root)

  @doc"""
  Copy to the store a page and all its references from the other store.
  In the case of pages on the network in a distributed store, this may
  be lazy.

  Returns store
  """
  def copy(store, other_store, hash)

  @doc"""
  Free a page referenced by its hash, marking it as no longer needed.

  Returns store
  """
  def free(store, hash)

  @doc"""
  Garbage collection: keep only these pages and their descendents
  """
  def gc(store, keep_roots)

  @doc"""
  true iff store has page
  """
  def has?(store, root)
end


defmodule MST.LocalStore do
  defstruct [:pages] 

  def new() do
    %__MODULE__{ pages: %{} }
  end

  defimpl MST.PageStore do
    def put(store, page) do
      hash = MST.term_hash page
      store = %{ store | pages: Map.put(store.pages, hash, page) }
      { hash, store }
    end

    def get(store, hash) do
      store.pages[hash]
    end

    def copy(store, other_store, hash) do
      page = MST.PageStore.get(other_store, hash)
      if page != nil do
        refs = MST.Page.refs(page)
        store = Enum.reduce(refs, store, fn x, acc -> copy(acc, other_store, x) end)
        %{ store | pages: Map.put(store.pages, hash, page) }
      else
        store
      end
    end

    def free(store, _hash) do
      # Do nothing, we GC instead
      store
    end

    def missing_set(%MST.LocalStore{pages: pages}=store, root) do
      case pages[root] do
        nil -> MapSet.new([root])
        x ->
          Enum.reduce(MST.Page.refs(x), MapSet.new(),
            fn x, acc -> MapSet.union(acc, missing_set(store, x)) end)
      end
    end

    def gc(%MST.LocalStore{pages: pages}, keep_roots) do
      pages2 = Enum.reduce(keep_roots, %{}, 
        fn x, acc -> gc_aux(acc, pages, x) end)
      %MST.LocalStore{pages: pages2}
    end

    defp gc_aux(set, from_pages, root) do
      page = from_pages[root]
      if page != nil and not Map.has_key?(set, root) do
        set = Map.put(set, root, page)
        Enum.reduce(MST.Page.refs(page), set,
          fn x, acc -> gc_aux(acc, from_pages, x) end)
      else
        set
      end
    end

    def has?(store, page) do
      Map.has_key?(store.pages, page)
    end
  end
end


