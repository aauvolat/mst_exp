defmodule MST.MerkleList do
  defstruct [:top, :root, :store, :cmp]

  @doc"""
  Create a Merkle list store.
  """
  def new(cmp) do
    root_item = :root
    root_hash = MST.term_hash root_item
    state = %__MODULE__{
              top:   root_hash,
              root:  root_hash,
              store: %{ root_hash => root_item },
              cmp:   cmp,
            }
    state
  end

  defp push(state, item) do
    new_item = {item, state.top}
    new_item_hash = MST.term_hash new_item
    new_store = Map.put(state.store, new_item_hash, new_item)
    %{ state | :top => new_item_hash, :store => new_store }
  end

  defp pop(state) do
    if state.top == state.root do
      :error
    else
      {item, next} = Map.get(state.store, state.top)
      new_store = Map.delete(state.store, state.top)
      new_state = %{ state | :top => next, :store => new_store }
      {:ok, item, new_state}
    end
  end

  @doc"""
  Insert a list of items in the store.

  A callback function may be specified that is called on any item
  that is sucessfully added, i.e. that wasn't present in the store before.
  """
  def insert_many(state, items) do
    items_sorted = Enum.sort(items, fn (x, y) -> state.cmp.(x, y) == :after end)
    insert_many_aux(state, items_sorted)
  end

  defp insert_many_aux(state, []) do
    state
  end

  defp insert_many_aux(state, [item | rest]) do
    case pop(state) do
      :error ->
        new_state = push(insert_many_aux(state, rest), item)
        new_state
      {:ok, front, state_rest} ->
        case state.cmp.(item, front) do
          :after ->
            push(insert_many_aux(state, rest), item)
          :duplicate -> insert_many_aux(state, rest)
          :before -> push(insert_many_aux(state_rest, [item | rest]), front)
        end
    end
  end

  @doc"""
  Insert a single item in the store.

  A callback function may be specified that is called on the item
  if it is sucessfully added, i.e. it wasn't present in the store before.
  """
  def insert(state, item) do
    insert_many(state, [item])
  end
end
