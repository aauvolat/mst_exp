defmodule MST do
  def merge_true(true, true), do: true

  def cmp_term(a, b) do
    cond do
      a > b -> :after
      a < b -> :before
      a == b -> :duplicate
    end
  end

  def term_hash(term, algo \\ :sha256) do
    :crypto.hash(algo, (:erlang.term_to_binary term))
  end
end
