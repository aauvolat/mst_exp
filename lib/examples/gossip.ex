defmodule Bignets.Examples.Gossip do
  @moduledoc"""
  Basic epidemic contamination.
  """

  def parse_args(args) do
    {opts, r, inv} = OptionParser.parse(args, strict: [
      fanout: :integer,
    ])
    if r != [] or inv != [] do
      IO.puts("Invalid arguments for Gossip")
      System.halt(1)
    end
    opts = put_in(opts[:fanout], opts[:fanout] || 6)
    opts
  end

  def init(node_id, net, opts) do
    if node_id == 0 do
      peers = Bignets.Network.sample_peers(net, opts[:fanout])
      net = Enum.reduce(peers, net, fn(peer, net) ->
                        Bignets.Network.send(net, peer, :contaminate)
        end)
      {{true, opts}, net}
    else
      {{false, opts}, net}
    end
  end

  def handle({state, opts}, :contaminate, net) do
    if state == true do
      # already contaminated, do nothing
      {{true, opts}, net}
    else
      peers = Bignets.Network.sample_peers(net, opts[:fanout])
      net = Enum.reduce(peers, net, fn(peer, net) ->
                        Bignets.Network.send(net, peer, :contaminate)
        end)
      {{true, opts}, net}
    end
  end

  def metrics({state, _opts}, net) do
    net
    |> Bignets.Network.save_metric("n_contaminated", Bignets.Metric.Sum.value(if state do 1 else 0 end))
  end
end
