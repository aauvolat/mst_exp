defmodule Bignets.Examples.GPS do
  @moduledoc"""
  Reproduce the results from the paper: "Speed for the elite, consistency for the masses"
  """

  def parse_args(args) do
    {opts, r, inv} = OptionParser.parse(args, strict: [
      nprimaries: :integer,
      interval: :integer,
      fanout: :integer,
    ])
    if r != [] or inv != [] do
      IO.puts("Invalid arguments for GPS")
      System.halt(1)
    end
    if opts[:nprimaries] == nil do
      IO.puts("Please specify --nprimaries for GPS")
      System.halt(1)
    end
    opts = put_in(opts[:fanout], opts[:fanout] || 10)
    opts = put_in(opts[:interval], opts[:interval] || 1)
    opts
  end

  def init(node_id, net, opts) do
    net = if node_id == 0 do
      Enum.reduce(0..9, net, fn (i, net) ->
        Bignets.Network.send(net, net.my_id, {:gossip, i}, i*opts[:interval])
      end)
    else
      net
    end
    if node_id < opts[:nprimaries] do
      {{:primary, opts, [], []}, net}
    else
      {{:secondary, opts, []}, net}
    end
  end

  def handle({:primary, opts, mA, mB}=state, {:gossip, i}, net) do
    cond do
      i not in mA ->
        mA = [i | mA]
        peers = Bignets.Network.sample_peers(net, opts[:fanout], 0..(opts[:nprimaries]-1))
        net = Enum.reduce(peers, net,
                    fn(peer, net) -> Bignets.Network.send(net, peer, {:gossip, i}) end)
        {{:primary, opts, mA, mB}, net}
      i in mA and i not in mB ->
        mB = [i | mB]
        peers = Bignets.Network.sample_peers(net, opts[:fanout], opts[:nprimaries]..(net.nproc-1))
        net = Enum.reduce(peers, net,
                    fn(peer, net) -> Bignets.Network.send(net, peer, {:gossip, i}) end)
        {{:primary, opts, mA, mB}, net}
      true -> {state, net}
    end
  end

  def handle({:secondary, opts, m}=state, {:gossip, i}, net) do
    cond do
      i not in m ->
        m = [i | m]
        peers = Bignets.Network.sample_peers(net, opts[:fanout], opts[:nprimaries]..(net.nproc-1))
        net = Enum.reduce(peers, net,
                    fn(peer, net) -> Bignets.Network.send(net, peer, {:gossip, i}) end)
        {{:secondary, opts, m}, net}
      true -> {state, net}
    end
  end

  def metrics({:primary, _opts, mA, _mB}, net) do
    fully_contaminated = if Enum.count(mA) == 10 do 1 else 0 end
    inconsistent = if mA != [] and Enum.max(mA)+1 != Enum.count(mA) do 1 else 0 end
    net
    |> Bignets.Network.save_metric("n_p_inconsistent", Bignets.Metric.Sum.value(inconsistent))
    |> Bignets.Network.save_metric("n_p_fc", Bignets.Metric.Sum.value(fully_contaminated))
  end

  def metrics({:secondary, _opts, mA}, net) do
    fully_contaminated = if Enum.count(mA) == 10 do 1 else 0 end
    inconsistent = if mA != [] and Enum.max(mA)+1 != Enum.count(mA) do 1 else 0 end
    net
    |> Bignets.Network.save_metric("n_s_inconsistent", Bignets.Metric.Sum.value(inconsistent))
    |> Bignets.Network.save_metric("n_s_fc", Bignets.Metric.Sum.value(fully_contaminated))
  end
end
