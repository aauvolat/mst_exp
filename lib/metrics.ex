defprotocol Bignets.Metric do
  def local_aggregate(m1, m2)
  def net_aggregate(m1, m2)
  def prev_aggregate(new, prev, nproc)
  def final_value(m, nproc)
end

defmodule Bignets.Metric.Sum do
  defstruct [:val]

  def value(x) do
    %__MODULE__{val: x}
  end

  defimpl Bignets.Metric do
    def local_aggregate(m1, m2) do
      %Bignets.Metric.Sum{val: m1.val + m2.val}
    end
    def net_aggregate(m1, m2) do
      %Bignets.Metric.Sum{val: m1.val + m2.val}
    end
    def prev_aggregate(new, _prev, _nproc) do
      new
    end
    def final_value(m, _nproc) do
      m.val
    end
  end
end

defmodule Bignets.Metric.CountDistinct do
  defstruct [:set]

  def item(x) do
    %__MODULE__{set: MapSet.new([x])}
  end

  defimpl Bignets.Metric do
    def local_aggregate(m1, m2) do
      newset = MapSet.union(m1.set, m2.set)
      %Bignets.Metric.CountDistinct{set: newset}
    end
    def net_aggregate(m1, m2) do
      newset = MapSet.union(m1.set, m2.set)
      %Bignets.Metric.CountDistinct{set: newset}
    end
    def prev_aggregate(new, _prev, _nproc) do
      new
    end
    def final_value(m, _nproc) do
      MapSet.size(m.set)
    end
  end
end

defmodule Bignets.Metric.Utils do
  def combine(i1, i2) do
    Enum.reduce(i1, i2, fn {k, v}, acc ->
      if Map.has_key?(acc, k) do
        %{acc | k => v + acc[k]}
      else
        Map.put(acc, k, v)
      end
    end)
  end

  def sum_entropy(dic, nproc) do
    dic
    |> Enum.map(fn {_k, v} ->
      frac = v / nproc
      if frac == 0 or frac == 1 do
        0
      else
        - frac * :math.log2(frac) - (1 - frac) * :math.log2(1 - frac)
      end
    end)
    |> Enum.sum()
  end
end

defmodule Bignets.Metric.DisseminationEntropy do
  defstruct [:items]

  def value(items) do
    dict = for x <- items, into: %{}, do: {x, 1}
    %__MODULE__{items: dict}
  end

  defimpl Bignets.Metric do
    def local_aggregate(m1, m2) do
      %Bignets.Metric.DisseminationEntropy{items:
          Bignets.Metric.Utils.combine(m1.items, m2.items)}
    end
    def net_aggregate(m1, m2) do
      %Bignets.Metric.DisseminationEntropy{items:
          Bignets.Metric.Utils.combine(m1.items, m2.items)}
    end
    def prev_aggregate(new, _prev, _nproc) do
      new
    end
    def final_value(m, nproc) do
      Bignets.Metric.Utils.sum_entropy(m.items, nproc)
    end
  end
end

defmodule Bignets.Metric.DisseminationEntropyDelta do
  defstruct [:items]

  def value(items) do
    dict = for x <- items, into: %{}, do: {x, 1}
    %__MODULE__{items: dict}
  end

  defimpl Bignets.Metric do
    def local_aggregate(m1, m2) do
      %Bignets.Metric.DisseminationEntropyDelta{items:
          Bignets.Metric.Utils.combine(m1.items, m2.items)}
    end
    def net_aggregate(m1, m2) do
      %Bignets.Metric.DisseminationEntropyDelta{items:
          Bignets.Metric.Utils.combine(m1.items, m2.items)}
    end
    def prev_aggregate(new, prev, nproc) do
      xx = if prev == nil do new.items else
        Bignets.Metric.Utils.combine(prev.items, new.items)
      end
      xx = for {k, v} <- xx, v != nproc, into: %{}, do: {k, v}
      %Bignets.Metric.DisseminationEntropyDelta{items: xx}
    end
    def final_value(m, nproc) do
      Bignets.Metric.Utils.sum_entropy(m.items, nproc)
    end
  end
end
