defmodule Mix.Tasks.Bignets.Sim do
  use Mix.Task

  @shortdoc "Run a simulation in Bignets"
  def run(args) do
    # :ets.new(:shared_pages, [:set, :public, :named_table])
    # :ets.new(:used_pages, [:set, :public, :named_table])

    {opts, args, invalid} = OptionParser.parse(args, strict: [
      nproc: :integer,
      sync: :boolean,
      latencies: :string,
      minlatency: :integer,
      tmax: :integer,
      out: :string,
    ])
    if invalid != [] do
      IO.puts("Invalid arguments:")
      for {k, v} <- invalid do
        IO.puts("#{k}: #{v}")
      end
      System.halt(1)
    end

    {mod, init} = case args do
      [mod | args2] ->
        mod = String.to_atom("Elixir." <> mod)
        {mod, apply(mod, :parse_args, [args2])}
      _ ->
        IO.puts("Usage: mix bignets.sim [options] -- module [module options]")
        System.halt(1)
    end

    nproc = opts[:nproc] || 1000
    sync = opts[:sync]

    {latencies_fn, min_latency, tmax} = cond do
      sync ->
        {fn (_, _) -> 1 end, 1, opts[:tmax] || 15}
      opts[:latencies] != nil ->
        min_latency = opts[:minlatency] || 20
        table = :ets.new(:latencies, [:set, :protected])
        {:ok, raw_data} = File.read(opts[:latencies])
        data = raw_data
               |> String.split("\n")
               |> Enum.filter(&(&1!=""))
               |> Enum.map(&(String.split(&1, "\t")))
        ndata = Enum.count(data)
        for {i, x} <- Enum.zip(0..(ndata-1), data) do
          x = Enum.filter(x, &(&1!=""))
          for {j, val} <- Enum.zip(0..(ndata-1), x) do
            {val, ""} = Float.parse val
            :ets.insert(table, {{i, j}, max(val, min_latency)})
          end
        end
        latency_fn = fn (i, j) ->
          key = {rem(i, ndata), rem(j, ndata)}
          case :ets.lookup(table, key) do
            [{_, l}] -> l
          end
        end
        {latency_fn, min_latency, opts[:tmax] || 10000}
      true ->
        {fn (_, _) -> 100 + :rand.uniform(100) end, 100, opts[:tmax] || 2000}
    end

    if opts[:out] do
      Process.flag(:trap_exit, true)
      {:ok, message_logger} = Bignets.FileLogger.start_link(opts[:out] <> ".msglog", name: :message_logger)
      {:ok, app_logger} = Bignets.FileLogger.start_link(opts[:out] <> ".applog", name: :app_logger)

      results = Bignets.sim_net(mod, init, nproc, latencies_fn, min_latency, tmax)

      GenServer.stop(message_logger)
      GenServer.stop(app_logger)
      
      IO.puts("Writing results to CSV file: #{opts[:out]}")
      keys = elem(List.first(results), 1) |> Map.keys() |> Enum.sort()
      header = Enum.join(keys, ",")
      data = results
      |> Enum.map(fn {t, metrics} ->
          metrics_str = keys
                        |> Enum.map(fn k -> "#{metrics[k]}" end)
                        |> Enum.join(",")
          "#{t},#{metrics_str}"
      end)
      |> Enum.join("\n")
      csv = "t,#{header}\n#{data}"
      File.write!(opts[:out], csv)
    else
      Bignets.sim_net(mod, init, nproc, latencies_fn, min_latency, tmax)
    end
  end
end
