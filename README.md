This repository contains the code for the simulations shown in the paper:

    Merkle Search Trees: Efficient State-Based CRDTs in Open Networks
    Alex Auvolat, François Taïani

The code is based on a generic simulation framework, Bignets, written in Elixir, whose code can also be found in this repository.


# Bignets

An efficient simulator for very large scale networks with asynchronous message passing and arbitrary link latency.

## Installation

Install Elixir. Clone this repo. Optionnally, run `mix compile` in this directory.

## Source layout

The simulator library code is in `lib/`

Some basic examples can be found in `lib/examples/`

`lib/mst` contains everything related to the Merkle data structures (Merkle Search Trees, Merkle Prefix Trees)

`lib/exp_ev_store` contains the code for the actual event store experiments.

## How to run the examples

Here are a few examples:

```
mix bignets.sim --nproc 10000 -- Bignets.Examples.Gossip
mix bignets.sim --nproc 10000 --sync --tmax 20 -- Bignets.Examples.GPS --nprimaries 1000
mix bignets.sim --nproc 10000 --tmax 3000 --out results.csv -- Bignets.Examples.GPS --nprimaries 1000 --interval 100 --fanout 6
```

**`--sync` versus real latency versus default**

In `--sync` mode, simulation steps are of duration 1 and all messages arrive after a delay of 1.

If `--latencies <data file>` is specified, a latency matrix will be loaded and used in the simulation. The latency matrix may be smaller than `--nproc`, in which case the values are used several times. The `--minlatency <deltat>` option can be specified (default: 20), which defines the duration of simulation steps. All latencies smaller than this lower bound are clipped to that value.

In default mode, simulation steps are of duration 100 and all messages arrive after a delay of 100 to 200 chosen randomly.


# Merkle Search Tree event store experiment

This repository implements an experiment with Merkle Search Trees to implement an event store.

Problem definition: nodes in the system randomly generate some events. We want all events to be delivered to all processes, and we want to minimize the network traffic for doing so.

Here are a few example runs:

```
mix bignets.sim --nproc 1000 --tmax 20000 -- Exp.EventStore.MST --nevgenerators 100
mix bignets.sim --nproc 1000 --tmax 13000 -- Exp.EventStore.MST --nevgenerators 10 --evranditv 1000 --lastevent 10000
mix bignets.sim --nproc 1000 --tmax 15000 -- Exp.EventStore.MST --nevgenerators 10 --evranditv 1000 --lastevent 10000 --fanout 6 --maxmerges 3 --maxsamemerge 1
mix bignets.sim --nproc 1000 --tmax 105000 -- Exp.EventStore.MST --nevgenerators 100 --evranditv 10000 --lastevent 100000 --fanout 6 --maxmerges 0
mix bignets.sim --nproc 1000 --tmax 105000 -- Exp.EventStore.MST --nevgenerators 100 --evranditv 10000 --lastevent 100000 --fanout 6 --maxmerges 1
```

The `Makefile` provided in the repository also contains a few example runs.

Here are the parameters accepted by the experiment:

- `--nevgenerators`: number of processes that generate events
- `--evminitv` and `--evranditv`: minimum interval and random interval between two randomly generated events. The event generating processes generate events with intervals `evminitv + rand [0,  evranditv)`
- `--lastevent`: time of the last event generated (optionnal). Enables observation of the network stabilising in a quiescent period.
- `--fanout`: fanout for single event gossip
- `--gossipitv`: minimum interval between two gossips
- `--maxmerges`: maximum number of peers a process may be gathering data from at the same time in order to merge state from them
- `--maxsamemerge`: maximum number of peers a process may be gathering data from at the same time that advertise the same state (i.e. same MST root hash)
- `--treemodule`: the tree module to use (MerkleSearchTree or MerklePrefixTree, MerkleSearchTree is better)

Here are the metrics reported by the algorithm for each time slice:

- `n_recv`: number of message receive events
- `n_send`: number of messages sent
- `n_events`: number of events generated in the whole network
- `n_merges`: number of merge operations that succeeded and in which new events were learned
- `n_merges_nc`: number of merge operations that succeeded and in which no new events were learned
- `n_abandons`: number of merge operations that are abandonned because some old pages were deleted and could not be found anymore
- `n_pending_merges`: number of merge operations in progress, waiting for data pages to be received
- `n_usefull_pages`: number of pages received by a process that they didn't already have in their data store
- `n_useless_pages`: number of pages received by a process that they already had in their data store
- `n_useless_gossip`: number of gossip messages for single events where the event was already known to the recipient
- `n_roots`: number of different roots present in the nodes at the end of the time slice, i.e. number of different states (sets of events already received) nodes can be in

Two competitors that do not ensure full message delivery are implemented.
They are not evaluated in the paper.
The first one is a simple infect and die gossip algorithm, while the second one is based on rumor mongering.
Here is how to run them:

```
mix bignets.sim --nproc 1000 --tmax 80000 -- Exp.EventStore.InfectAndDie --nevgenerators 100 --evranditv 10000 --lastevent 70000 --fanout 20
mix bignets.sim --nproc 1000 --tmax 80000 -- Exp.EventStore.RumorMongering --nevgenerators 100 --evranditv 10000 --lastevent 70000 --fanout 10 --gossipitv 200 --timeout 4000
```


The main competitor is ScuttleButt, which uses a vector clock to compare two data sets.
With this method, full message delivery is achieved.
In our paper, we show that this method has higher bandwith usage when there are many nodes and when the event rate is low, because of the metadata that consists in a full vector clock that is exchanged at each anti-entropy round.
Here is an example of how to run this algorithm:

```
mix bignets.sim --nproc 1000 --tmax 80000 -- Exp.EventStore.ScuttleButt --nevgenerators 100 --evranditv 10000 --lastevent 70000
```

The main code for our gossip algorithm based on Merkle Search Trees is in `lib/exp_ev_store/mst.ex`, whereas the code for the ScuttleButt algorithm is in `lib/exp_ev_store/scuttlebutt.ex`.
The Merkle Search Tree data structure itself is found in `lib/mst/mst.ex`.

## Experimental parameters used in the paper

The script `gen_exp.py` will generate a `exp.make` script (in the form of a makefile) that runs all the experiments whose results are shown in the paper, and many other experiments as well.
Figures 2 and 4 and Table III correspond to the experiment we call `esx10`, while Figure 3 and table IV correspond to the experiments `esx11` and `esx13`. Warning: the Merkle Prefix Tree experiment `esx13` did not terminate on our machines, run it at your own risks.

```
./gen_exp.py                            # generate exp.make
make -f exp.make                        # run all experiments (don't do this)
make -f exp.make esx10                  # run the experiments for configuration esx10
make -f exp.make exp/esx13_mst1.csv     # run one of the algorithm parameters for esx13
```

The notebook `exp/esx.ipynb` contains the code to exploit the `.csv` files output by the experiments and plot the graphs shown in the paper.
The file `exp/esx.py` contains the code of `esx.ipynb` exported as a simple Python script, however it is not meant to be run as is.


