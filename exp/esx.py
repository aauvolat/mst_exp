#!/usr/bin/env python
# coding: utf-8

# In[1]:


import matplotlib
import numpy as np
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
from collections import defaultdict


# In[2]:


data_cache = {}
def get_data(file):
    global data_cache
    if not file in data_cache:
        data = np.genfromtxt(file, delimiter=',', skip_header=1)
        C = {k: i for i, k in enumerate(next(open(file)).strip().split(','))}
        data_cache[file] = (data, C)
    return data_cache[file]


# In[3]:


from scipy.signal import gaussian

def gsmoothen(data, std, mode='same'):
    w = gaussian(4*std, std=std)
    return np.convolve(data, w/w.sum(), mode)

def myplot(title, files, smooth_std, what):
    plt.figure(figsize=(16,5))
    plt.subplot(1, 3, 1)
    for (f, name) in files:
        data, C = get_data(f)
        try:
            plt.plot(data[:, C['t']], what(data, C), label=name)
        except KeyError:
            continue
    plt.legend()
    plt.title(title)
    
    if smooth_std > 0:
        plt.subplot(1, 3, 2)
        for (f, name) in files:
            data, C = get_data(f)
            try:
                if False:
                    plt.plot(data[:, C['t']], gsmoothen(what(data, C), smooth_std), label=name)
                else:
                    x = data[:, C['t']]
                    imax = len(x) - (len(x) % smooth_std)
                    x = x[:imax].reshape((-1, smooth_std)).mean(axis=1)
                    y = what(data, C)[:imax].reshape((-1, smooth_std)).mean(axis=1)
                    plt.plot(x, y, label=name)
            except KeyError:
                continue
        plt.legend()
        plt.title(title + " (smoothed)")

    means = []
    plt.subplot(1, 3, 3)
    plt.plot([-0.5, len(files)+2.5], [0, 0], 'k--')
    for i, (f, name) in enumerate(files):
        data, C = get_data(f)
        try:
            mean = np.mean(what(data, C))
        except KeyError:
            continue
        means.append((name, mean))
        plt.fill([i, i, i+0.5, i+0.5], [0, mean, mean, 0], label=name)
    plt.xlim(-0.5, len(files)+2.5)
    plt.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)
    plt.legend()
    plt.title(title + " (average)")
        
    plt.show()
    return means


def myplot_xy(title, xname, yname, files, what):
    fmts = {'SB':'*--', 'MPT':'*-.', 'MST':'*:'}
    colors = {'SB': 'darkorange', 'MPT': 'royalblue', 'MST': 'green'}
    fullname = {'SB': 'Scuttlebutt', 'MPT': 'MPT', 'MST': 'MST (ours)'}
    plt.figure(figsize=(4,3))
    exps = defaultdict(list)
    for (f, name) in files:
        data, C = get_data(f)
        try:
            xval, yval = what(data, C)
            exps[name.split(' ')[0]].append((xval, yval))
        except KeyError:
            continue
    for i, (expname, expvals) in enumerate(exps.items()):
        plt.plot([v[0] for v in expvals], [v[1] for v in expvals], fmts[expname], color=colors[expname], label=fullname[expname])
    plt.legend()
    plt.xlabel(xname)
    plt.ylabel(yname)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    #plt.xticks([0, max(max(v[0] for v in exp) for _, exp in exps.items())])
    plt.locator_params(nbins=3, axis='x')
    plt.title(title)
    plt.tight_layout()
    plt.savefig(files[0][0].split('_')[0]+'.pdf')
    plt.show()
    


# In[4]:


def pretty_table(data):
    def dmin(x):
        return min(x) if len(x) > 0 else None
    def dmax(x):
        return max(x) if len(x) > 0 else None
    CW = 12
    cols = [x[0] for x in data]
    hcols = [''] + cols
    print(' | '.join([x.ljust(CW) for i, x in enumerate(hcols)]))
    print('-' * ((len(hcols))*(CW+3)))
    rows = [x[0] for x in data[0][1]]
    vals = {}
    for (cn, cl) in data:
        for (rn, v) in cl:
            vals[(cn, rn)] = v
    vmin = {cn: dmin([v for ((cn1, _), v) in vals.items() if cn1 == cn]) for cn in cols}
    vmax = {cn: dmax([v for ((cn1, _), v) in vals.items() if cn1 == cn]) for cn in cols}
    for rn in rows:
        rstr = rn.ljust(CW)
        for cn in cols:
            rstr += ' | '
            if (cn, rn) in vals:
                v = vals[(cn, rn)]
                sp = ' -' if v == vmin[cn] else (' +' if v == vmax[cn] else '')
                fmt = '{: >10,.4f}' if vmax[cn] < 1000 else '{: >10,.0f}'
                rstr += (fmt.format(v).ljust(CW-2) + sp).ljust(CW)
            else:
                rstr += ' '*CW
        print(rstr)


# In[5]:



def theplots(fileset, smooth_std):    
    global data_cache
    data_cache = {}
    evt = myplot("Events", fileset, smooth_std,
           lambda d, C: d[:, C['n_events']])
    myplot("Total events", fileset, smooth_std,
           lambda d, C: np.cumsum(d[:, C['n_events']]))
    nst = myplot("Number of different node states", fileset, smooth_std,
           lambda d, C: d[:, C['n_roots']])
    ent = myplot("Entropy", fileset, smooth_std,
           lambda d, C: d[:, C['entropy']])
    nby = myplot("Number of bytes sent", fileset, smooth_std,
           lambda d, C: d[:, C['b_sent']] + 28*d[:, C['n_sent']])
    nms = myplot("Number of messages sent", fileset, smooth_std,
           lambda d, C: d[:, C['n_sent']])
    nhx = myplot("Number of hashes exchanged", fileset, smooth_std,
           lambda d, C: d[:, C['n_pages+']] + d[:, C['n_pages-']])
    #nab = myplot("Proportion of abandons", fileset, smooth_std,
    #       lambda d, C: d[:, C['n_abandons']] / (1+d[:, C['n_abandons']]+d[:, C['n_merges+']]+d[:, C['n_merges-']]))
    myplot_xy("Trade-off", "Entropy of round (average)", "Bytes sent per round (average)", fileset, 
           lambda d, C: (d[:, C['entropy']].mean(), (d[:, C['b_sent']] + 28*d[:, C['n_sent']]).mean()))
    pretty_table([('events/round', evt), ('states', nst), ('entropy', ent), ('bytes sent', nby), ('msg sent', nms), ('hashes xchg', nhx)])


# In[6]:


theplots([
    ("esx01_sb025.csv", "SB 0.25"),
    ("esx01_sb05.csv", "SB 0.5"),
    #("esx01_sb1.csv", "SB 1"),
    ("esx01_sb2.csv", "SB 2"),
    ("esx01_sb4.csv", "SB 4"),
    #("esx01_sb8.csv", "SB 8"),
    ("esx01_mpt1.csv", "MPT 1"),
    ("esx01_mpt2.csv", "MPT 2"),
    ("esx01_mpt3.csv", "MPT 3"),
    ("esx01_mpt4.csv", "MPT 4"),
    ("esx01_mpt6.csv", "MPT 6"),
    #("20181128/esx01_mst1nsg.csv", "MST 1 old"),
    ("esx01_mst1.csv", "MST 1"),
    ("esx01_mst2.csv", "MST 2"),
    ("esx01_mst3.csv", "MST 3"),
    ("esx01_mst4.csv", "MST 4"),
    ("esx01_mst6.csv", "MST 6"),
    ("esx01_mst8.csv", "MST 8"),
], 50)


# In[7]:


theplots([
    ("esx02_sb025.csv", "SB 0.25"),
    ("esx02_sb05.csv", "SB 0.5"),
    ("esx02_sb1.csv", "SB 1"),
    ("esx02_sb2.csv", "SB 2"),
    ("esx02_sb4.csv", "SB 4"),
    ("esx02_sb8.csv", "SB 8"),
    ("esx02_mpt1.csv", "MPT 1"),
    ("esx02_mpt2.csv", "MPT 2"),
    ("esx02_mpt3.csv", "MPT 3"),
    ("esx02_mpt4.csv", "MPT 4"),
    #("esx02_mpt6.csv", "MPT 6"),
    #("esx02_mpt8.csv", "MPT 8"),
    ("esx02_mst1.csv", "MST 1"),
    ("esx02_mst2.csv", "MST 2"),
    ("esx02_mst3.csv", "MST 3"),
    ("esx02_mst4.csv", "MST 4"),
    ("esx02_mst6.csv", "MST 6"),
    ("esx02_mst8.csv", "MST 8"),
], 10)


# In[8]:


theplots([
    ("esx03_sb025.csv", "SB 0.25"),
    #("esx03_sb05.csv", "SB 0.5"),
    #("esx03_sb1.csv", "SB 1"),
    ("esx03_sb2.csv", "SB 2"),
    ("esx03_sb4.csv", "SB 4"),
    #("esx03_sb8.csv", "SB 8"),
    ("esx03_mpt1.csv", "MPT 1"),
    ("esx03_mpt2.csv", "MPT 2"),
    ("esx03_mpt3.csv", "MPT 3"),
    ("esx03_mpt4.csv", "MPT 4"),
    ("esx03_mst1.csv", "MST 1"),
    ("esx03_mst2.csv", "MST 2"),
    ("esx03_mst3.csv", "MST 3"),
    #("esx03_mst4.csv", "MST 4"),
    ("esx03_mst6.csv", "MST 6"),
    #("esx03_mst8.csv", "MST 8"),
], 500)


# In[9]:


theplots([
    ("esx05_sb025.csv", "SB 0.25"),
    ("esx05_sb05.csv", "SB 0.5"),
    ("esx05_sb1.csv", "SB 1"),
    ("esx05_sb2.csv", "SB 2"),
    #("esx05_sb4.csv", "SB 4"),
    #("esx05_sb8.csv", "SB 8"),
    ("esx05_mpt1.csv", "MPT 1"),
    ("esx05_mpt2.csv", "MPT 2"),
    ("esx05_mpt3.csv", "MPT 3"),
    ("esx05_mpt4.csv", "MPT 4"),
    #("esx05_mst0.csv", "mst0"),
    ("esx05_mst1.csv", "MST 1"),
    ("esx05_mst2.csv", "MST 2"),
    ("esx05_mst3.csv", "MST 3"),
    ("esx05_mst4.csv", "MST 4"),
    ("esx05_mst6.csv", "MST 6"),
    ("esx05_mst8.csv", "MST 8"),
], 100)


# In[10]:


theplots([
    ("esx06_sb025.csv", "SB 0.25"),
    ("esx06_sb05.csv", "SB 0.5"),
    ("esx06_sb1.csv", "SB 1"),
    ("esx06_sb2.csv", "SB 2"),
    ("esx06_sb4.csv", "SB 4"),
    #("esx06_sb8.csv", "SB 8"),
    ("esx06_mpt1.csv", "MPT 1"),
    ("esx06_mpt2.csv", "MPT 2"),
    ("esx06_mpt3.csv", "MPT 3"),
    #("esx06_mpt4.csv", "MPT 4"),
    #("esx06_mpt6.csv", "MPT 6"),
    #("esx06_mpt8.csv", "MPT 8"),
    ("esx06_mst1.csv", "MST 1"),
    ("esx06_mst2.csv", "MST 2"),
    ("esx06_mst3.csv", "MST 3"),
    ("esx06_mst4.csv", "MST 4"),
    ("esx06_mst6.csv", "MST 6"),
    #("esx06_mst8.csv", "MST 8"),
], 10)


# In[11]:


theplots([
    #("esx10_sb0125.csv", "SB 1/8"),
    #("esx10_sb025.csv", "SB 1/4"),
    #("esx10_sb05.csv", "SB 1/2"),
    ("esx10_sb1.csv", "SB 1"),
    ("esx10_sb2.csv", "SB 2"),
    ("esx10_sb4.csv", "SB 4"),
    #("esx10_sb8.csv", "SB 8"),
    ("esx10_mpt1.csv", "MPT 1"),
    ("esx10_mpt2.csv", "MPT 2"),
    ("esx10_mpt3.csv", "MPT 3"),
    ("esx10_mpt4.csv", "MPT 4"),
    ("esx10_mpt6.csv", "MPT 6"),
    ("esx10_mst1.csv", "MST 1"),
    ("esx10_mst2.csv", "MST 2"),
    ("esx10_mst3.csv", "MST 3"),
    ("esx10_mst4.csv", "MST 4"),
    #("esx10_mst4.csv.old", "MST 4 (old)"),
    #("esx10_mst4.csv.old2", "MST 4 (old2)"),
    ("esx10_mst6.csv", "MST 6"),
], 500)


# In[12]:


theplots([
    ("esx11_sb0125.csv", "SB 1/8"),
    ("esx11_sb025.csv", "SB 1/4"),
    ("esx11_sb05.csv", "SB 1/2"),
    ("esx11_sb1.csv", "SB 1"),
    ("esx11_sb2.csv", "SB 2"),
    #("esx11_sb4.csv", "SB 4"),
    #("esx11_sb8.csv", "SB 8"),
    ("esx11_mpt1.csv", "MPT 1"),
    ("esx11_mpt2.csv", "MPT 2"),
    ("esx11_mpt3.csv", "MPT 3"),
    ("esx11_mpt4.csv", "MPT 4"),
    ("esx11_mpt6.csv", "MPT 6"),
    ("esx11_mst1.csv", "MST 1"),
    ("esx11_mst2.csv", "MST 2"),
    ("esx11_mst3.csv", "MST 3"),
    ("esx11_mst4.csv", "MST 4"),
    #("esx11_mst6.csv", "MST 6"),
    ("esx11_mst8.csv", "MST 8"),
], 500)


# In[13]:


theplots([
    #("esx12_sb0125.csv", "SB 1/8"),
    #("esx12_sb025.csv", "SB 1/4"),
    #("esx12_sb05.csv", "SB 1/2"),
    ("esx12_sb1.csv", "SB 1"),
    ("esx12_sb2.csv", "SB 2"),
    ("esx12_sb4.csv", "SB 4"),
    #("esx12_sb8.csv", "SB 8"),
    ("esx12_mpt1.csv", "MPT 1"),
    ("esx12_mpt2.csv", "MPT 2"),
    ("esx12_mpt3.csv", "MPT 3"),
    ("esx12_mpt4.csv", "MPT 4"),
    #("esx12_mpt6.csv", "MPT 6"),
    ("esx12_mst1.csv", "MST 1"),
    ("esx12_mst2.csv", "MST 2"),
    ("esx12_mst3.csv", "MST 3"),
    ("esx12_mst4.csv", "MST 4"),
    ("esx12_mst6.csv", "MST 6"),
], 500)


# In[14]:


theplots([
    ("esx13_sb0125.csv", "SB 1/8"),
    ("esx13_sb025.csv", "SB 1/4"),
    ("esx13_sb05.csv", "SB 1/2"),
    ("esx13_sb1.csv", "SB 1"),
    ("esx13_sb2.csv", "SB 2"),
    #("esx13_sb4.csv", "SB 4"),
    #("esx13_sb8.csv", "SB 8"),
    #("esx13_mpt1.csv", "MPT 1"),
    #("esx13_mpt2.csv", "MPT 2"),
    #("esx13_mpt3.csv", "MPT 3"),
    #("esx13_mpt4.csv", "MPT 4"),
    #("esx13_mpt6.csv", "MPT 6"),
    ("esx13_mst1.csv", "MST 1"),
    ("esx13_mst2.csv", "MST 2"),
    ("esx13_mst3.csv", "MST 3"),
    ("esx13_mst4.csv", "MST 4"),
    #("esx13_mst6.csv", "MST 6"),
], 500)


# In[ ]:


def graph_msg_arrival_time(applog_files, bins):
    data = []
    names = []
    for file, name in applog_files:
        tbl = np.genfromtxt(file+".csv.applog", delimiter=',')
        itvs = tbl[:, 1] - tbl[:, 3]
        print("99%: ", name, np.percentile(itvs, 99))
        data.append(itvs)
        names.append(name)
    plt.hist(data, bins=bins, label=names)
    plt.legend()
    plt.show()
graph_msg_arrival_time([
    ("test_mst", "MST"),
    ("test_mpt", "MPT"),
    ("test_sb", "SB"),
], np.linspace(0, 1000, 10))


# In[ ]:


graph_msg_arrival_time([
    ("esx10_mst4", "MST4"),
    ("esx10_mpt4", "MPT4"),
    ("esx10_sb2", "SB2"),
], np.linspace(0, 100, 20))


# In[ ]:


graph_msg_arrival_time([
    ("esx11_mst4", "MST4"),
    ("esx11_mpt4", "MPT4"),
    ("esx11_sb025", "SB1/4"),
], np.linspace(0, 100, 20))


# In[ ]:


graph_msg_arrival_time([
    ("esx13_mst4", "MST4"),
    ("esx13_sb025", "SB1/4"),
], np.linspace(0, 100, 20))


# In[ ]:


theplots([
    ("esx10_sb2.csv", "SB 2"),
    ("esx10_mpt4.csv", "MPT 4"),
    ("esx10_mst4.csv", "MST 4"),
], 2000)


# In[ ]:


#fmts = ['--', '-.', ':']
colors = ['darkorange', 'royalblue', 'green']
plt.figure(figsize=(4,3))
ymax = 0
files = [
    ("esx10_sb2.csv", "Scuttlebutt"),
    ("esx10_mpt4.csv", "MPT"),
    ("esx10_mst4.csv", "MST (ours)"),
]
for i, (f, name) in enumerate(files):
    data, C = get_data(f)
    try:
        time = data[:, C['t']][:38000]
        yinit = (data[:, C['b_sent']] + 28*data[:, C['n_sent']])[:38000]
        fewsmooth = 10
        imax = len(yinit) - (len(yinit) % fewsmooth)
        yfew = yinit[:imax].reshape((-1, fewsmooth)).mean(axis=1)
        ymax = max(ymax, np.max(yfew))
        plt.plot(time[:len(yinit) - (len(yinit) % fewsmooth):fewsmooth], yfew, alpha=0.2, color=colors[i])
    except KeyError:
        continue
for i, (f, name) in enumerate(files):
    data, C = get_data(f)
    try:
        time = data[:, C['t']][:38000]
        yinit = (data[:, C['b_sent']] + 28*data[:, C['n_sent']])[:38000]
        if True:
            ydata = gsmoothen(yinit, 500, mode='valid')
            diff = len(yinit)-len(ydata)
            xdata = time[diff//2:diff//2+len(ydata)]
        else:
            ydata = yinit[:imax].reshape((-1, 1000)).mean(axis=1)
            xdata = time[:imax].reshape((-1, 1000)).mean(axis=1)
        plt.plot(xdata, ydata, color=colors[i], label=name)
    except KeyError:
        continue
plt.legend()
plt.ylim((0, ymax/2))
#plt.xticks([])
#plt.yticks([])
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.xlabel("Time (rounds)")
plt.ylabel("Bandwith usage (bytes per round)")
plt.tight_layout()
plt.savefig("esx10_bw.pdf")
plt.show()


# In[ ]:


theplots([
    ("esx11_sb025.csv", "SB 1/4"),
    ("esx11_mpt4.csv", "MPT 4"),
    ("esx11_mst4.csv", "MST 4"),
], 500)


# In[ ]:


theplots([
    ("esx13_sb025.csv", "SB 1/4"),
    ("esx13_mst4.csv", "MST 4"),
], 500)


# In[ ]:


def graph_nevents(applog_files):
    data = []
    names = []
    for file, name in applog_files:
        tbl = np.genfromtxt(file+".csv.msglog", delimiter=',')
        what = np.sort(tbl[:, 2:4], axis=0)
        _, vals = np.unique(what, axis=0, return_counts=True)
        data.append(vals)
        names.append(name)
    plt.hist(data, label=names)
    plt.legend()
    plt.show()
graph_nevents([
    ("test_mst", "MST"),
    ("test_mpt", "MPT"),
    ("test_sb", "SB"),
])


# In[ ]:


graph_nevents([
    #("esx10_mst4", "MST4"),
    #("esx10_mpt4", "MPT4"),
    ("esx10_sb2", "SB2"),
])


# In[5]:


configs = {
    'esx01': (1000, 2, False, 'Scuttlebutt is better'),
    #'esx02': (400, 8, False, None),
    'esx03': (1000, 0.5, True, 'MST is better'),
    'esx05': (1000, 2, False, None),
    'esx06': (1000, 20, False, None),
    'esx10': (1000, 0.1, True, None),
    'esx11': (1000, 1, None, 'Ex-aequo (figure 3a)'),
    'esx12': (2000, 0.1, True, None),
    'esx13': (2000, 1, True, None),
    'esx20': (500, 10, False, None),
    'esx21': (500, 1, False, None),
    #'esx22': (500, 1/1.5, False, None),
    'esx23': (500, 1/2., False, None),
    'esx24': (500, 1/3., None, None),
    'esx25': (500, 1/4., True, None),
    'esx26': (500, 1/10., True, None),
}
xmin, xmax = 300, 2100
plt.figure(figsize=(4,3))
for xp, (nnodes, nmsg, better, label) in configs.items():
    c = {False: "red", True: "green", None: "black"}
    plt.semilogy([nnodes], [nmsg], '+', color=c[better], label=label)
xrange = np.linspace(xmin, xmax, 42)
boundary = xrange * 4 / 3000 - 1./3.
plt.fill_between(xrange, boundary, 100, alpha=0.2, color='red')
plt.fill_between(xrange, 0, boundary, alpha=0.2, color='green')
plt.loglog(xrange, boundary, 'k--', linewidth=1, label='Theoretical boundary')
plt.xlim(xmin, xmax)
plt.ylim(0.05, 50)
plt.xlabel('Number of nodes')
plt.ylabel('Events per round')
plt.legend(prop={"size":7})
plt.tight_layout()
plt.savefig('boundary.pdf')
plt.plot()


# In[46]:


for esx in ['20', '21', '22', '23', '24', '25', '26']:
    print("============== BEGIN ESX {} ============".format(esx))
    theplots([
        ("esx{}_sb025.csv".format(esx), "SB 0.25"),
        ("esx{}_sb05.csv".format(esx), "SB 0.5"),
        ("esx{}_sb1.csv".format(esx), "SB 1"),
        ("esx{}_sb2.csv".format(esx), "SB 2"),
        ("esx{}_sb4.csv".format(esx), "SB 4"),
        ("esx{}_sb8.csv".format(esx), "SB 8"),
        ("esx{}_mst1.csv".format(esx), "MST 1"),
        ("esx{}_mst2.csv".format(esx), "MST 2"),
        ("esx{}_mst3.csv".format(esx), "MST 3"),
        ("esx{}_mst4.csv".format(esx), "MST 4"),
        ("esx{}_mst6.csv".format(esx), "MST 6"),
        ("esx{}_mst8.csv".format(esx), "MST 8"),
    ], 50)
    print("============== END ESX {} ============".format(esx))


# In[ ]:




