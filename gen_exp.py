#!/usr/bin/env python3

import itertools
import functools


def argprod(*args):
    def c_arg(arg):
        if isinstance(arg, str):
            return[('', arg)]
        elif isinstance(arg, list):
            return arg
        else:
            raise "Invalid"
    args = itertools.product(*(c_arg(arg) for arg in args))
    def flat(arg):
        return functools.reduce( (lambda a, b: (a[0]+b[0], a[1]+' '+b[1])), arg)
    args = [flat(arg) for arg in args]
    return args

def argflatten(d):
    ret = []
    for k, dd in d:
        ret = ret + [(k + ddn, ddv) for ddn, ddv in dd]
    return ret

C_common = "--sync"
D_common = "--evminitv 1"

exps = [
        ("esx01",
            {"C": "--nproc 1000 --tmax 2200",
             "D": "--nevgenerators 1000 --evranditv 1000 --lastevent 1900"}),
        ("esx02",
            {"C": "--nproc 400 --tmax 1000",
             "D": "--nevgenerators 400 --evranditv 100 --lastevent 900"}),
        ("esx03", 
            {"C": "--nproc 1000 --tmax 20000",
             "D": "--nevgenerators 1000 --evranditv 10000 --lastevent 19000"}),
        ("esx05",
            {"C": "--nproc 1000 --tmax 10000",
             "D": "--nevgenerators 1000 --evranditv 1000 --lastevent 9800"}),
        ("esx06",
            {"C": "--nproc 1000 --tmax 4000",
             "D": "--nevgenerators 1000 --evranditv 100 --lastevent 3500"}),

        ("esx10",
            {"C": "--nproc 1000 --tmax 40000",
             "D": "--nevgenerators 1000 --evranditv 20000 --lastevent 39000"}),
        ("esx11",
            {"C": "--nproc 1000 --tmax 10000",
             "D": "--nevgenerators 1000 --evranditv 2000 --lastevent 9000"}),
        ("esx12",
            {"C": "--nproc 2000 --tmax 60000",
             "D": "--nevgenerators 10000 --evranditv 40000 --lastevent 59000"}),
        ("esx13",
            {"C": "--nproc 2000 --tmax 10000",
             "D": "--nevgenerators 10000 --evranditv 4000 --lastevent 9000"}),

        ("esx20",
            {"C": "--nproc 400 --tmax 2000",
             "D": "--nevgenerators 500 --evranditv 100 --lastevent 1900"}),
        ("esx21",
            {"C": "--nproc 400 --tmax 2000",
             "D": "--nevgenerators 500 --evranditv 1000 --lastevent 1900"}),
        ("esx22",
            {"C": "--nproc 400 --tmax 2000",
             "D": "--nevgenerators 500 --evranditv 1500 --lastevent 1900"}),
        ("esx23",
            {"C": "--nproc 400 --tmax 2000",
             "D": "--nevgenerators 500 --evranditv 2000 --lastevent 1900"}),
        ("esx24",
            {"C": "--nproc 400 --tmax 3000",
             "D": "--nevgenerators 500 --evranditv 3000 --lastevent 2900"}),
        ("esx25",
            {"C": "--nproc 400 --tmax 4000",
             "D": "--nevgenerators 500 --evranditv 4000 --lastevent 3900"}),
        ("esx26",
            {"C": "--nproc 400 --tmax 10000",
             "D": "--nevgenerators 500 --evranditv 10000 --lastevent 9900"}),
]

pset = argflatten([
    ("sb", argprod(
        "Exp.EventStore.ScuttleButt",
        [("1", "--fanout 1 --gossipitv 1"),
         ("2", "--fanout 2 --gossipitv 1"),
         ("4", "--fanout 4 --gossipitv 1"),
         ("8", "--fanout 8 --gossipitv 1"),
         ("05", "--fanout 1 --gossipitv 2"),
         ("025", "--fanout 1 --gossipitv 4"),
         ("0125", "--fanout 1 --gossipitv 8"),
         ("205", "--fanout 2 --gossipitv 2"),
         ("2025", "--fanout 2 --gossipitv 4")]
    )),
    ("m", argprod(
        "Exp.EventStore.MST --fanout 6 --backlog 100 --nosinglegossip --gossipitv 1",
        [("st", "--treemodule MerkleSearchTree"),
         ("pt", "--treemodule MerklePrefixTree")
	],
        [("1", "--maxmerges 1"),
         ("2", "--maxmerges 2"),
         ("3", "--maxmerges 3"),
         ("4", "--maxmerges 4"),
         ("6", "--maxmerges 6"),
         ("8", "--maxmerges 8")]
    )),
])

all_exp = argflatten([
        (exp_k + "_",
	 argprod(
            C_common,
            exp_v["C"],
            "--",
            pset,
            exp_v["D"],
            D_common
        ))
        for exp_k, exp_v in exps
    ])


exps_names = [n for n, _ in exps]

makefile = open("exp.make", "w")
makefile.write(".PHONY: all {}\n".format(" ".join(exps_names)))
makefile.write("all: {}\n".format(" ".join(exps_names)))
for en in exps_names:
    makefile.write("{}: {}\n".format(en, " ".join("exp/{}.csv".format(x) for (x, _) in all_exp if x.startswith(en))))
for n, v in all_exp:
    makefile.write("exp/{}.csv:\n".format(n))
    makefile.write("\tmix bignets.sim --out $@ {}\n".format(v))
makefile.close()

print(open("exp.make", "r").read())
